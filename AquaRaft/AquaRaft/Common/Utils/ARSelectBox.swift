//
//  ARSelectBox.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/3/26.
//

import UIKit

class ARSelectBox: UIButton {

    init() {
        super.init(frame: .zero)
        
        self.setImage(.init(named: "common_select_box_unselected"), for: .normal)
        self.setImage(.init(named: "common_select_box_selected"), for: .selected)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSizeMake(16, 16)
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            return self.sizeThatFits(.zero)
        }
    }
    
}
