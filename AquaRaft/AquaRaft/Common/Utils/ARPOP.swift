//
//  ARPOP.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/1.
//

import Foundation
import UIKit

public struct ARPOP<Base> {
    let base: Base
    init(_ base: Base) {
        self.base = base
    }
}

public protocol ARPOPCompatible {}

public extension ARPOPCompatible {
    
    static var ar: ARPOP<Self>.Type {
        get{ ARPOP<Self>.self }
        set {}
    }
    
    var ar: ARPOP<Self> {
        get { ARPOP(self) }
        set {}
    }
}
