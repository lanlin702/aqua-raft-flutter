//
//  UIWindow+AR.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/10.
//

import Foundation
import UIKit

extension ARPOP where Base: UIWindow {
    
    func topViewController() -> UIViewController? {
        guard var topVc = self.base.rootViewController else {
            return nil
        }
        
        while topVc.presentedViewController != nil {
            topVc = topVc.presentedViewController!
        }
        return topVc
    }
    
}
