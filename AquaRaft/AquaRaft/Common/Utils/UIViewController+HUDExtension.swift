//
//  UIViewController+HUDExtension.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/1.
//

import UIKit

extension UIViewController: ARPOPCompatible {}
extension ARPOP where Base: UIViewController {
    /// 展示loading
    /// - Parameter duration: 时长 default：0
    func showLoading(duration: TimeInterval = 0) {
        
        self.base.view.ar.showLoading(duration: duration)
    }
    
    /// 展示toast
    func makeToast(_ msg: String) {
        self.base.view.ar.makeToast(msg)
    }
    
    /// 隐藏
    func hideHUD() {
        self.base.view.ar.hideHUD()
    }
}
