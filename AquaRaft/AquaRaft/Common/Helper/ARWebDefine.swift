//
//  ARWebDefine.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/3/27.
//

import Foundation
import UIKit
import SafariServices

class ARWebDefine {
    private init() {
        // Do nothing...
    }
    
    
    enum url: String {
    case termOfUse = "https://h5.cosmofusionly.xyz/termConditions.html"
    case privacyPolicy = "https://h5.cosmofusionly.xyz/privacyPolicy.html"
    }
    
    static func presentSFVc(_ urlStr: String, at vc: UIViewController) {
        if let url = URL(string: urlStr) {
            let safariVc = SFSafariViewController(url: url)
            safariVc.modalPresentationStyle = .overFullScreen
            vc.present(safariVc, animated: true)
        }
    }
}
