//
//  ARFileManager.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/6.
//

import Foundation

class ARFileHelper {
    
    private init() {
        // Do nothing...
    }
    
    // MARK: - File method
    
    /// 记录路径
    static let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.path
    
    static let audioPathName = "AR_audio"
    static let imagePathName = "AR_image"
    
    /// 返回录音地址
    /// 返回: document/AR_audio
    static func localAudioPath() -> String {
        let path = "\(documentsDirectory)/\(audioPathName)"
        if FileManager.default.fileExists(atPath: path) == false {
            // 不存在 - 创建
            try? FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: true)
        }
        return path
    }
    
    static func createAudioPath(_ fileExtension: String = "m4a") -> String {
        // 立马创建一个路径
        let path = localAudioPath()
        let name = String(Date().timeIntervalSince1970)
        let audioPath = "\(path)/\(name).\(fileExtension)"
        if FileManager.default.fileExists(atPath: audioPath) {
            // 删除
            try? FileManager.default.removeItem(atPath: audioPath)
        }
        return audioPath
    }
    
    
    /// 返回图片地址
    /// 返回: document/AR_image
    static func localImagePath() -> String {
        let path = "\(documentsDirectory)/\(imagePathName)"
        if FileManager.default.fileExists(atPath: path) == false {
            // 不存在 - 创建
            try? FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: true)
        }
        return path
    }
    
    static func createImagePath(_ fileExtension: String = "jpeg") -> String {
        // 立马创建一个路径
        let path = localImagePath()
        let name = String(Date().timeIntervalSince1970)
        let imagePath = "\(path)/\(name).\(fileExtension)"
        if FileManager.default.fileExists(atPath: imagePath) {
            // 删除
            try? FileManager.default.removeItem(atPath: imagePath)
        }
        return imagePath
    }
}
