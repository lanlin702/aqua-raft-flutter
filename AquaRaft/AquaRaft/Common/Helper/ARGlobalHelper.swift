//
//  ARGlobalHelper.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/3/24.
//

import JKSwiftExtension

// 其他
class ARHelper {
    
    private init() {
        // Do nothing...
    }
    
    // MARK: - 基础的宏
    
    static func screenHeight() -> CGFloat {
        return UIScreen.main.bounds.size.height
    }
    
    static func screenWidth() -> CGFloat {
        return UIScreen.main.bounds.size.width
    }
    
    static func isIphoneX() -> Bool {
        var isPhoneX : Bool = false
        isPhoneX = ((UIApplication.shared.delegate?.window??.safeAreaInsets.bottom ?? 0.0) == 0.0) ? false : true
    //    isPhoneX = UIApplication.shared.windows.first?.safeAreaInsets.bottom == 0.0 ? false : true
        return isPhoneX
    }
    
    static func safeBottomHeight() -> CGFloat {
        return isIphoneX() ? 34.0 : 0.0
    }
    
    static func tabBarHeight() -> CGFloat {
        return 49.0 + safeBottomHeight()
    }
    
    static func statusBarHeight() -> CGFloat {
        var statusBarHeight : CGFloat = 0.0
        if #available(iOS 13.0, *) {
            statusBarHeight = UIApplication.shared.delegate?.window??.windowScene?.statusBarManager?.statusBarFrame.size.height ?? 0.0;
            if #available(iOS 16.0, *) {
                // fix: 一些机型获取有问题
                let needAdjust = (statusBarHeight == 44 || statusBarHeight == 54)
                if needAdjust && (UIApplication.shared.delegate?.window??.windowScene?.keyWindow?.safeAreaInsets.top ?? 0) == 59 {
                    statusBarHeight = 59
                }
            }
        } else {
            statusBarHeight = UIApplication.shared.statusBarFrame.height;
        }
        return statusBarHeight
    }
    
    static func navigationHeight() -> CGFloat {
        return self.statusBarHeight() + 44.0
    }
    
    static func window() -> UIWindow {
        return (UIApplication.shared.delegate?.window ?? UIWindow.init())!
    }
    
    /// 设计稿的比例
    static func heightScale() -> CGFloat {
        return 812.0 / Self.screenHeight()
    }
    static func widthScale() -> CGFloat {
        return 375.0 / Self.screenWidth()
    }
    
    static func heightScale(_ height: CGFloat) -> CGFloat {
        return height * Self.heightScale()
    }
    static func widthScale(_ width: CGFloat) -> CGFloat {
        return width * Self.widthScale()
    }
    
    // MARK: - 业务上的宏
    
    /// 字体
    static func font(_ size: CGFloat, weight: UIFont.Weight) -> UIFont {
        // 暂时写死 - 后续可以统一修改
        return .systemFont(ofSize: size, weight: weight)
    }
    
    /// 国际化
    static func localString(_ str: String) -> String {
        return str
    }
    
    // MARK: - 信息
    
    static func appName() -> String {
        if let appName = Bundle.main.infoDictionary?["CFBundleName"] as? String {
            return appName
        }
        return ""
    }
    
    static func bundleId() -> String {
        if let bundleID = Bundle.main.bundleIdentifier {
            return bundleID
        }
        /// 按道理应该没有空的情况
        fatalError("Bundle id is nil")
    }
    
    /// 版本号: 1.0.0
    static func versionCode() -> String {
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            return version
        }
        /// 按道理应该没有空的情况
        fatalError("versionCode is nil")
    }
    
    /// 数字的buildcode : 999
    static func buildCode() -> String {
        if let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            return build
        }
        /// 按道理应该没有空的情况
        fatalError("buildCode is nil")
    }
    
    /// 系统中设置的国家码
    static func systemContryCode() -> String {
        if #available(iOS 16, *) {
            return Locale.current.language.region?.identifier ?? "Unknown"
        } else {
            return Locale.current.regionCode ?? "Unknown"
        }
    }
    
    /// 系统中设置的语言码
    static func systemLanguageCode() -> String {
        if #available(iOS 16, *) {
            return Locale.current.language.languageCode?.identifier ?? "Unknown"
        } else {
            return Locale.current.languageCode ?? "Unknown"
        }
    }
    
    // MARK: - Helper
    
    static func afterAsyncInMainQueue(_ completion: @escaping () -> Void) {
        let random = TimeInterval.random(in: 0.5...1.5)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + random) {
            completion()
        }
    }
}

// MARK: - ARRequestHelper

class ARRequestHelper {
    
    private init() {
        // Do nothing...
    }
    
    // MARK: -
    
    /// 返回通用请求头
    static func commonHeaders() -> [String: String] {
        let uuid = ARDevice.share.uuidResult.idfa ?? ""
        var commonHeaders: [String: String] = [
            "device-id" : uuid,
            "platform" : "iOS",
            "model" : ARDevice.share.phoneModel,
            "pkg" : ARHelper.bundleId(),
            "ver" : ARConfigManager.share.ver,
            "p_ver" : ARHelper.buildCode(),
            "kst" : "1",
            "sys_lan" : ARHelper.systemLanguageCode(),
            "lang" : ARHelper.systemLanguageCode(),
        ]
        if let token = ARUserManager.shared.assentToken {
            commonHeaders["Authorization"] = "Bearer\(token)"
        }
        return commonHeaders
    }
    
}

