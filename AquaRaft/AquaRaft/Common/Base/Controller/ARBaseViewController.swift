//
//  ARBaseViewController.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/3/22.
//

import UIKit
import JKSwiftExtension

/**
 控制器基类
 */
class ARBaseViewController: UIViewController {
    
    init() {
        super.init(nibName: nil, bundle: nil)
        self.modalPresentationStyle = .fullScreen
        self.fd_prefersNavigationBarHidden = false
//        self.hidesBottomBarWhenPushed = true
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        self.modalPresentationStyle = .fullScreen
        self.fd_prefersNavigationBarHidden = false
//        self.hidesBottomBarWhenPushed = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        
        // setup
        self.edgesForExtendedLayout = [UIRectEdge.bottom, UIRectEdge.left, UIRectEdge.right]
    }
    
    // MARK: - Action
    
    @objc func ar_transitionToBack() {
        if self.presentingViewController != nil {
            self.dismiss(animated: true)
            return
        }
        
        self.navigationController?.popViewController(animated: true)
    }
}

/// 设置navigation的方法
extension ARBaseViewController {
    
    /// 设置背景图片
    func ar_setupNavBgImg() {
        
        guard let nav = self.navigationController as? ARNavigationController else {
            return
        }
        var name = "nav_bg"
        if let path = Bundle.main.path(forResource: name, ofType: "png") {
            let image = UIImage.init(contentsOfFile: path)
            nav.setupNavImage(image)
//            nav.navigationBar.setBackgroundImage(image, for: .default)
        }
    }
    
    /// 设置白色的返回按钮
    func ar_configWhiteBackItem() {
        // nav
        let backItem = UIBarButtonItem(image: .init(named: "common_nav_new_back_white_icon")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(ar_transitionToBack))
        self.navigationItem.leftBarButtonItem = backItem
    }
    
    /// 设置黑色的返回按钮
    func ar_configBlackBackItem() {
        // nav
        let backItem = UIBarButtonItem(image: .init(named: "common_nav_new_back_icon")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(ar_transitionToBack))
        self.navigationItem.leftBarButtonItem = backItem
    }
    
}
