//
//  ARNavigationController.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/3/24.
//

import UIKit

/// 全局的 nav
class ARNavigationController: UINavigationController {

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        self.modalPresentationStyle = .fullScreen
    }
    
    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        
        self.modalPresentationStyle = .fullScreen
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    private var isViewWillLayoutSubViews = false
    
    private lazy var customImgView: UIImageView = {
        let view = UIImageView()
        view.backgroundColor = .white
        view.contentMode = .scaleAspectFill
        view.layer.masksToBounds = true
        return view
    }()
    
    // override
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if isViewWillLayoutSubViews == false {
            isViewWillLayoutSubViews = true
            
            // custom view
            if let backgroundView = self.navigationBar.subviews.first {
                backgroundView.insertSubview(customImgView, at: 0)
                customImgView.frame = .init(x: 0, y: 0, width: self.navigationBar.jk.width, height: (ARHelper.navigationHeight()))
            }
        }
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        if self.children.count > 0 {
            viewController.hidesBottomBarWhenPushed = true
        }
        super.pushViewController(viewController, animated: animated)
    }
    
    // function
    
    func setupNavImage(_ image: UIImage?) {
        customImgView.image = image
    }
    
}
