//
//  ARRequestError.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/3/31.
//

import Foundation
import Alamofire

/// 错误
enum ARRequestError: Error, LocalizedError {
    
    /// 接口返回的错误
    case serviceError(error: AFError)
    /// 服务器返回非0的错误
    case apiError(code: Int, msg: String)
    /// 模型转换失败
    case modelConvertError(error: Error?)
    /// 常规error
    case normalError
    
    /// 错误
    var errorDescription: String? {
        switch self {
        case.apiError(_, let msg):
            return msg
        case .serviceError(let error):
            return error.localizedDescription
        case .modelConvertError(let error):
            if let error = error {
                return error.localizedDescription
            }
            return "Model convert failed"
        case .normalError:
            return "Request failed!"
        }
    }
    
}

/// 结果
enum ARRequestResult<ResponseModel: Decodable> {
    
    case faild(err: ARRequestError)
    
    case successWithoutResponse
    case success(model: ResponseModel)
}
