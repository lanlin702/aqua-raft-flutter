//
//  ARRefreshHeader.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/17.
//

import UIKit
import MJRefresh

class ARRefreshHeader: MJRefreshNormalHeader {

    override func prepare() {
        super.prepare()
        
        if ARHelper.isIphoneX() {
            // 刘海屏适配
            self.mj_h = MJRefreshHeaderHeight + ARHelper.statusBarHeight()
            self.ignoredScrollViewContentInsetTop = -ARHelper.statusBarHeight() * 0.5
        } else {
            self.mj_h = MJRefreshHeaderHeight
        }
        
        self.lastUpdatedTimeLabel?.isHidden = true
        self.stateLabel?.isHidden = true
    }

}
