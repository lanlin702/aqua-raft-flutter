//
//  ARTabBarItemContentView.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/25.
//

import UIKit
import ESTabBarController_swift

class ARTabBarItemContentView: ESTabBarItemContentView {
    
    override var iconColor: UIColor {
        didSet {
            // Do nothing...
        }
    }
    
    override var highlightIconColor: UIColor {
        didSet {
            // Do nothing...
        }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        imageView.tintColor = nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateDisplay() {
        imageView.image = (selected ? (selectedImage ?? image) : image)
//        imageView.tintColor = selected ? highlightIconColor : iconColor
        titleLabel.textColor = selected ? highlightTextColor : textColor
        backgroundColor = selected ? highlightBackdropColor : backdropColor
    }
    
}
