//
//  ARTabControllerDelegate.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/9.
//

import Foundation

/// 刷新操作
protocol ARTabControllerRefresh {
    
    /// 刷新
    func refreshData()
    
}
