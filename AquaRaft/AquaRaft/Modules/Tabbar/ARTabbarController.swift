//
//  ARTabbarController.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/2.
//

import Foundation
import UIKit
import ESTabBarController_swift

class ARTabbarController: ESTabBarController {
    
    // MARK: - Private Property
    
    private let tabbarBackgroundView = UIView()
    
    private var lastSelectedIndex: Int = -1
    
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTabbarUI()
        self.setupControllers()
        
        self.delegate = self
    }
    
    // MARK: - UI
    
    private func setupTabbarUI() {
        let apperarance = self.tabBar.standardAppearance
        apperarance.backgroundColor = .white
        apperarance.backgroundEffect = nil
        apperarance.shadowImage = UIImage()
        self.tabBar.barTintColor = .white
        self.tabBar.standardAppearance = apperarance
        
        tabbarBackgroundView.layer.cornerRadius = 24
        tabbarBackgroundView.backgroundColor = .white
        tabbarBackgroundView.layer.shadowColor = UIColor.black.cgColor
        tabbarBackgroundView.layer.shadowOffset = CGSize(width: 0, height: -1)
        tabbarBackgroundView.layer.shadowRadius = 24
        tabbarBackgroundView.clipsToBounds = false
        tabbarBackgroundView.layer.shadowOpacity = 0.2
        tabBar.addSubview(tabbarBackgroundView)

        tabBar.isTranslucent = true
        tabBar.backgroundColor = UIColor.clear
        tabBar.backgroundImage = UIImage()
        tabBar.shadowImage = UIImage()
        tabBar.layer.cornerRadius = 24
        tabBar.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]

//        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: ARHelper.screenWidth(), height: 0.5))
//        view.backgroundColor = UIColor.white
//        UITabBar.appearance().insertSubview(view, at: 0)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tabbarBackgroundView.frame = tabBar.bounds
    }
    
    /// 设置controller
    private func setupControllers() {
        
        let indexItemContentView = ARTabBarItemContentView()
        let indexVc = ARNavigationController(rootViewController: ARIndexViewController(nibName: nil, bundle: nil))
        indexVc.tabBarItem = ESTabBarItem(indexItemContentView, image: .init(named: "tabbar_index_unselected")?.withRenderingMode(.alwaysOriginal), selectedImage: .init(named: "tabbar_index_selected")?.withRenderingMode(.alwaysOriginal))

        let videoFlowItemContentView = ARTabBarItemContentView()
        let videoFlowVc = ARNavigationController(rootViewController: ARVideoFlowViewController(nibName: nil, bundle: nil))
        videoFlowVc.tabBarItem = ESTabBarItem(videoFlowItemContentView, image: .init(named: "tabbar_videoFlow_unselected")?.withRenderingMode(.alwaysOriginal), selectedImage: .init(named: "tabbar_videoFlow_selected")?.withRenderingMode(.alwaysOriginal))

        let discoverItemContentView = ARTabBarItemContentView()
        let discoverVc = ARNavigationController(rootViewController: ARDiscoverController(nibName: nil, bundle: nil))
        discoverVc.tabBarItem = ESTabBarItem(discoverItemContentView, image: .init(named: "tabbar_discover_unselected")?.withRenderingMode(.alwaysOriginal), selectedImage: .init(named: "tabbar_discover_selected")?.withRenderingMode(.alwaysOriginal))

        let mineItemContentView = ARTabBarItemContentView()
        let mineVc = ARNavigationController(rootViewController: ARMineViewController(nibName: nil, bundle: nil))
        mineVc.tabBarItem = ESTabBarItem(mineItemContentView, image: .init(named: "tabbar_mine_unselected")?.withRenderingMode(.alwaysOriginal), selectedImage: .init(named: "tabbar_mine_selected")?.withRenderingMode(.alwaysOriginal))
        
//        let indexVc = ARNavigationController(rootViewController: ARIndexViewController(nibName: nil, bundle: nil))
//        indexVc.tabBarItem = ESTabBarItem(image: .init(named: "tabbar_index_unselected")?.withRenderingMode(.alwaysOriginal), selectedImage: .init(named: "tabbar_index_selected")?.withRenderingMode(.alwaysOriginal))
//        let videoFlowVc = ARNavigationController(rootViewController: ARVideoFlowViewController(nibName: nil, bundle: nil))
//        videoFlowVc.tabBarItem = ESTabBarItem(image: .init(named: "tabbar_videoFlow_unselected")?.withRenderingMode(.alwaysOriginal), selectedImage: .init(named: "tabbar_videoFlow_selected")?.withRenderingMode(.alwaysOriginal))
//        let discoverVc = ARNavigationController(rootViewController: ARDiscoverController(nibName: nil, bundle: nil))
//        discoverVc.tabBarItem = ESTabBarItem(image: .init(named: "tabbar_discover_unselected")?.withRenderingMode(.alwaysOriginal), selectedImage: .init(named: "tabbar_discover_selected")?.withRenderingMode(.alwaysOriginal))
//        let mineVc = ARNavigationController(rootViewController: ARMineViewController(nibName: nil, bundle: nil))
//        mineVc.tabBarItem = ESTabBarItem(image: .init(named: "tabbar_mine_unselected")?.withRenderingMode(.alwaysOriginal), selectedImage: .init(named: "tabbar_mine_selected")?.withRenderingMode(.alwaysOriginal))
        
        self.viewControllers = [indexVc, videoFlowVc, discoverVc, mineVc]
    }
    
}

extension ARTabbarController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let index = tabBarController.selectedIndex
        let selectedViewController = viewController
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(cancellastIndex), object: nil)
        if lastSelectedIndex == index {
            self.cancellastIndex()
            
            // 刷新
            guard let nav = selectedViewController as? UINavigationController,
                    let refreshVc = nav.children.first as? ARTabControllerRefresh else {
                return
            }
            
            // 刷新
            refreshVc.refreshData()
            return
        }
        self.lastSelectedIndex = index
        self.perform(#selector(cancellastIndex), with: nil, afterDelay: 1)
    }
    
    /// 将 lastSelectedIndex 设为-1
    @objc private func cancellastIndex() {
        lastSelectedIndex = -1
    }
    
}
