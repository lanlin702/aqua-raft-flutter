//
//  ARIndexViewModel.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/7.
//

import Foundation

class ARIndexViewModel {
    
    // MARK: - Public Property
    
    /// 数据源
    var dataSource: [ARWorkModel] = []
    
    
    // MARK: - Action
    
    func fetchIndexWorks(_ completion: @escaping (ARDataCenterError?) -> Void) {
        ARDataCenter.default.fetchRecommendationList { [weak self] workList, err in
            guard let self = self else {
                return
            }
            
            if let err = err {
                completion(err)
                return
            }
            
            dataSource.removeAll()
            if let workList = workList {
                if workList.count > 5 {
                    // 取前5个
                    dataSource.appends(Array(workList[0...4]))
                } else {
                    dataSource.appends(workList)
                }
            }
            completion(nil)
        }
    }
}
