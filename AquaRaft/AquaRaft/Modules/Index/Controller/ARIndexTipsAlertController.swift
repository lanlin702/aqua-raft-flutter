//
//  ARIndexTipsAlertController.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/10.
//

import UIKit
import AttributedString
import SnapKit

class ARIndexTipsAlertController: ARBaseModalAlertController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.baseContainerView.addSubview(containerBgView)
        containerBgView.addSubview(contentLabel)
        containerBgView.addSubview(sureBtn)
        
        containerBgView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.equalTo(315)
            make.height.equalTo(354)
            make.edges.equalToSuperview()
        }
        
        contentLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(48)
            make.left.equalToSuperview().offset(24)
            make.right.equalToSuperview().offset(-24)
        }
        
        sureBtn.snp.makeConstraints { make in
            make.width.equalTo(180)
            make.height.equalTo(40)
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-40)
        }
    }
    
    @objc private func sureBtnClick(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
    // MARK: - Lazy
    
    private lazy var containerBgView: UIImageView = {
        let view = UIImageView(image: .init(named: "index_tips_alert_bg"))
        view.isUserInteractionEnabled = true
        return view
    }()
    
    private lazy var contentLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        
        let str = ARHelper.localString("Welcome to view the recommended list of famous attractions! Let’s give thumbs up to the exciting and fun rafting attractions!\n\nYou can also click the add button on the right to recommend more exciting and fun rafting places for users!")
        
        let att = ASAttributedString.init(string: str, .font(ARHelper.font(14, weight: .regular)),
            .foreground(.init(hexString: "#404040") ?? .black))
        label.attributed.text = "\(wrap: att, .paragraph(.alignment(.center), .lineSpacing(5)))"
        
        return label
    }()
    
    private lazy var sureBtn: UIButton = {
        let btn = UIButton.ar.themeStyleButton(height: 40, font: ARHelper.font(16, weight: .regular))
        btn.addTarget(self, action: #selector(sureBtnClick(_:)), for: .touchUpInside)
        btn.setTitle(ARHelper.localString("OK"), for: .normal)
        return btn
    }()
}
