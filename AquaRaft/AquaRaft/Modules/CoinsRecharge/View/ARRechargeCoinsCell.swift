//
//  ARRechargeCoinsCell.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/20.
//

import UIKit
import JKSwiftExtension
import SnapKit
import AttributedString

class ARRechargeCoinsCell: UICollectionViewCell {
    
    var model: ARRechargeCoinsModel? {
        didSet {
            guard let model = model else {
                return
            }
            self.updateValue(model)
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        self.contentView.addSubview(bgImageView)
        self.contentView.addSubview(coinIconView)
        self.contentView.addSubview(coinLabel)
        self.contentView.addSubview(discountLabel)
        self.contentView.addSubview(priceLabel)
        self.contentView.addSubview(hotBtn)
        self.contentView.addSubview(bigDealBtn)
        
        bgImageView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        coinIconView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(27)
            make.left.equalToSuperview().offset(32)
            make.width.equalTo(46)
            make.height.equalTo(45)
        }
        
        coinLabel.snp.makeConstraints { make in
            make.left.equalTo(coinIconView.snp.right).offset(12)
            make.top.equalTo(coinIconView).offset(7)
            make.height.equalTo(17)
        }
        
        discountLabel.snp.makeConstraints { make in
            make.left.equalTo(coinLabel)
            make.top.equalTo(coinLabel.snp.bottom).offset(6)
            make.height.equalTo(10)
        }
        
        priceLabel.snp.makeConstraints { make in
            make.right.equalToSuperview().offset(-8)
            make.bottom.equalToSuperview().offset(-12)
        }
        
        hotBtn.snp.makeConstraints { make in
            make.centerY.equalTo(self.contentView.snp.top)
            make.right.equalToSuperview()
            make.height.equalTo(18)
            make.width.equalTo(50)
        }
        bigDealBtn.snp.makeConstraints { make in
            make.centerY.equalTo(self.contentView.snp.top)
            make.right.equalToSuperview().offset(4)
            make.height.equalTo(20)
            make.width.equalTo(64)
        }
    }
    
    private func updateValue(_ model: ARRechargeCoinsModel) {
        discountLabel.isHidden = true
        hotBtn.isHidden = true
        bigDealBtn.isHidden = true
        
        coinLabel.text = String(model.exchangeCoin)
        if model.discount > 0 {
            discountLabel.isHidden = false
            discountLabel.text = "\(Int(model.discount * 100.0))% off"
        }
        
        let priceStr = "$\(model.price)  "
        var priceAttri = ASAttributedString(
                string: priceStr,
                .font(ARHelper.font(14, weight: .medium)),
                .foreground(UIColor(hexString: "#404040") ?? .black))
        if model.originalPrice != model.price {
            let originalPriceStr = "$\(model.originalPrice)"
            let attri = ASAttributedString(
                string: originalPriceStr,
                .font(ARHelper.font(10, weight: .regular)),
                .foreground(.black.withAlphaComponent(0.5)),
                .strikethrough(.single, color: .black.withAlphaComponent(0.5)))
            priceAttri = priceAttri + attri
        }
        priceLabel.attributed.text = "\(wrap: priceAttri, .paragraph(.alignment(.right)))"
        
        if let tags = model.tags {
            if tags.lowercased() == "hot" {
                hotBtn.isHidden = false
            } else {
                bigDealBtn.isHidden = false
            }
        }
    }
    
    // MARK: -
    
    private lazy var bgImageView: UIImageView = {
        let view = UIImageView()
//        let width = (ARHelper.screenWidth() - 16 * 2 - 7) / 2
//        let height = 124.0
        view.image = .init(named: "recharge_item_bg")?.resizableImage(withCapInsets: .init(top: 20, left: 20, bottom: 20, right: 20), resizingMode: .stretch)
//        view.jk.effectViewWithAlpha(alpha: 0.8, size: .init(width: width, height: height), style: .light)
//        view.layer.cornerRadius = 12
//        view.layer.masksToBounds = true
        return view
    }()
    
    private lazy var coinIconView: UIImageView = {
        let view = UIImageView(image: .init(named: "common_coin_icon"))
        return view
    }()
    
    private lazy var coinLabel: UILabel = {
        let label = UILabel()
        label.font = ARHelper.font(20, weight: .medium)
        label.textColor = .init(hexString: "#1D4B5A")
        return label
    }()
    
    private lazy var discountLabel: UILabel = {
        let label = UILabel()
        label.font = ARHelper.font(9, weight: .regular)
        label.textColor = .init(hexString: "#FF3EC8")
        label.isHidden = true
        return label
    }()
    
    private lazy var priceLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    private lazy var bigDealBtn: UIButton = {
        let btn = UIButton.ar.themeStyleButton(height: 20, font: ARHelper.font(12, weight: .regular))
        btn.setTitle(ARHelper.localString("Big Deal"), for: .normal)
        btn.isUserInteractionEnabled = false
        btn.isHidden = true
        return btn
    }()
    
    private lazy var hotBtn: UIButton = {
        let btn = UIButton(type: .custom)
        btn.backgroundColor = UIColor(hexString: "#FF8DDE")
        btn.setTitle(ARHelper.localString("HOT"), for: .normal)
        btn.setImage(.init(named: "recharge_hot_tag_icon"), for: .normal)
        btn.titleLabel?.font = ARHelper.font(10, weight: .bold)
        btn.layer.cornerRadius = 10
        btn.isUserInteractionEnabled = false
        btn.isHidden = true
        return btn
    }()
}
