//
//  ARRechargeCoinsApi\.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/20.
//

import Foundation
import Alamofire

class ARFetchRechargeCoinsListRequest: ARRequest {
    
    override var api: String {
        "/coin/goods/search"
    }
    
    override var httpMethod: HTTPMethod {
        .post
    }
    
    func fetchRechageCoinsList(_ completion: @escaping (ARRequestResult<[ARRechargeCoinsModel]>) -> Void) {
        self.parameters = [
            "isIncludeSubscription" : false,
            "payChannel" : "IAP"
        ]
        self.request(of: [ARRechargeCoinsModel].self) { response, result in
            completion(result)
        }
    }
}

// MARK: - 订单相关

class ARCreateRechargeOrderRequest: ARRequest {
    
    override var api: String {
        "/coin/recharge/create"
    }
    
    override var httpMethod: HTTPMethod {
        .post
    }
    
    override var encoder: ParameterEncoding {
        JSONEncoding.default
    }
    
    func createOrder(_ goodsCode: String, completion: @escaping (ARRequestResult<ARRechargeOrderModel>) -> Void) {
        self.parameters = [
            "entry" : "",
            "goodsCode" : goodsCode,
            "payChannel" : "IAP",
            "source" : ""
        ]
        self.request(of: ARRechargeOrderModel.self) { response, result in
            completion(result)
        }
    }
    
}

/// 支付凭证校验
class ARIAPReceiptVerificationRequest: ARRequest {
    
    override var api: String {
        "/coin/recharge/payment/ipa"
    }
    
    override var httpMethod: HTTPMethod {
        .post
    }
    
    override var encoder: ParameterEncoding {
        JSONEncoding.default
    }
    
    func verify(_ orderNo: String, payload: String, transcationId: String, completion: @escaping (Error?) -> Void) {
        self.parameters = [
            "orderNo": orderNo,
            "payload" : payload,
            "transactionId" : transcationId,
            "type" : "1"
        ]
        self.request { response, result in
            switch result {
            case .faild(let err):
                completion(err)
            case .successWithoutResponse, .success(_):
                completion(nil)
            }
        }
    }
    
}
