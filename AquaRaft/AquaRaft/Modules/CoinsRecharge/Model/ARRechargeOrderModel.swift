//
//  ARRechargeOrderModel.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/20.
//

import Foundation

struct ARRechargeOrderModel: Decodable {
    
    /*
     {
     "goodsCode": "61200",
     "goodsName": "100 Coins",
     "orderNo": "40619e5143204c28a8e81998408abc17",
     "payAmount": 0.99,
     "paidAmount": 0.99,
     "paidCurrency": "USD"
     }
     */
    
    let goodsCode: String
    let goodsName: String
    let orderNo: String
    let payAmount: Double
    let paidCurrency: String
}
