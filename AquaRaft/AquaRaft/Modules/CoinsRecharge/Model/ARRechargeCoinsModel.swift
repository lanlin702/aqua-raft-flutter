//
//  ARRechargeCoinsModel.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/20.
//

import Foundation

class ARRechargeCoinsModel: Decodable {

    /*
     {
     "goodsId": "54aaedcb-4c5f-4452-b67f-76e37fdac19e",
     "code": "61205",
     "icon": "6",
     "type": "0",
     "subType": 0,
     "tags": "Big Deal",
     "discount": 0.35,
     "originalPrice": 99.99,
     "price": 99.99,
     "exchangeCoin": 15000,
     "originalExchangeCoin": 0,
     "originalPriceRupee": 9600.0,
     "priceRupee": 9600.0,
     "localPaymentPriceRupee": 9000.0,
     "isPromotion": false,
     "localPayOriginalPrice": 9600,
     "localPayPrice": 9000
     }
     */
    
    let goodsId: String
    let icon: String
    let code: String
    let tags: String?
    let discount: Double
    
    let originalPrice: Double
    let price: Double
    let exchangeCoin: Int
}
