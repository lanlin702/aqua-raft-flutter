//
//  ARLoginAgreementAlert.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/3/27.
//

import UIKit
import AttributedString
import SnapKit
import SafariServices

class ARLoginAgreementAlert: ARBaseModalAlertController {

    var agreeCallback: (() -> Void)?
    
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.baseContainerView.addSubview(containerView)
        containerView.addSubview(logoView)
        containerView.addSubview(nameLable)
        containerView.addSubview(agreementLabel)
        containerView.addSubview(agreeBtn)
        containerView.addSubview(cancelBtn)
        
        containerView.snp.makeConstraints { make in
            make.width.equalTo(315)
            make.edges.equalToSuperview()
        }
        
        logoView.snp.makeConstraints { make in
            make.width.height.equalTo(64)
            make.top.equalToSuperview().offset(20)
            make.centerX.equalToSuperview()
        }
        
        nameLable.snp.makeConstraints { make in
            make.top.equalTo(logoView.snp.bottom).offset(13)
            make.height.equalTo(14)
            make.centerX.equalToSuperview()
        }
        
        agreementLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(24)
            make.right.equalToSuperview().offset(-24)
            make.top.equalTo(nameLable.snp.bottom).offset(31)
        }
        
        agreeBtn.snp.makeConstraints { make in
            make.top.equalTo(agreementLabel.snp.bottom).offset(24)
            make.width.equalTo(180)
            make.height.equalTo(40)
            make.centerX.equalToSuperview()
        }
        
        cancelBtn.snp.makeConstraints { make in
            make.top.equalTo(agreeBtn.snp.bottom).offset(20)
            make.bottom.equalToSuperview().offset(-24)
            make.width.equalTo(180)
            make.height.equalTo(40)
            make.centerX.equalToSuperview()
        }
    }
    
    // MARK: - Action

    @objc private func agreeBtnClick(_ sender: UIButton) {
        // 直接回调
        self.dismiss(animated: true) { [weak self] in
            self?.agreeCallback?()
        }
    }
    
    @objc private func cancelBtnClick(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
    private func termOfUse() {
        ARWebDefine.presentSFVc(ARWebDefine.url.termOfUse.rawValue, at: self)
    }
    
    private func privacyPolicy() {
        ARWebDefine.presentSFVc(ARWebDefine.url.privacyPolicy.rawValue, at: self)
    }
    
    
    // MARK: - Lazy
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 18
        return view
    }()
    
    private lazy var logoView: UIImageView = {
        let image = UIImage(contentsOfFile: Bundle.main.path(forResource: "logo.png", ofType: nil) ?? "")
        let view = UIImageView.init(image: image)
        view.contentMode = .scaleToFill
        return view
    }()
    
    private lazy var nameLable: UILabel = {
        let label = UILabel()
        label.textColor = .init(hexString: "#404040")
        label.font = ARHelper.font(12, weight: .regular)
        label.text = ARHelper.appName()
        return label
    }()
    
    private lazy var agreeBtn: UIButton = {
        let btn = UIButton.ar.themeStyleButton(height: 40, font: ARHelper.font(14, weight: .medium))
        btn.addTarget(self, action: #selector(agreeBtnClick(_:)), for: .touchUpInside)
        btn.setTitle(ARHelper.localString("Agree and Continue"), for: .normal)
        return btn
    }()
    
    private lazy var cancelBtn: UIButton = {
        let btn = UIButton(type: .custom)
        btn.addTarget(self, action: #selector(cancelBtnClick(_:)), for: .touchUpInside)
        btn.setTitle(ARHelper.localString("Cancel"), for: .normal)
        btn.titleLabel?.font = .systemFont(ofSize: 14, weight: .regular)
        btn.setTitleColor(.init(hexString: "#A9A9A9"), for: .normal)
        
//        // bgimg
        btn.setBackgroundImage(UIImage.jk.image(color: .init(hexString: "#F4F4F4")!), for: .normal)
        btn.layer.cornerRadius = 20
        btn.layer.masksToBounds = true
        
        return btn
    }()
    
    private lazy var agreementLabel: UILabel = {
        //By using our App you agree with our  Term of Use  and Privacy Policy.
        let label = UILabel()
        label.numberOfLines = 0
        
        var str: ASAttributedString = .init(string: ARHelper.localString("By using our App you agree with our\n"), .font(ARHelper.font(12, weight: .regular)), .foreground(.init(hexString: "#505050") ?? .black))
        str += ASAttributedString.init(string: ARHelper.localString("Term of Use"), .font(ARHelper.font(12, weight: .regular)), .foreground(.init(hexString: "#4DB4FF") ?? .black), .action { [weak self] in
            self?.termOfUse()
        })
        str += ASAttributedString.init(string: ARHelper.localString("  and "), .font(ARHelper.font(12, weight: .regular)), .foreground(.init(hexString: "#505050") ?? .black))
        str += ASAttributedString.init(string: ARHelper.localString("Privacy Policy."), .font(ARHelper.font(12, weight: .regular)), .foreground(.init(hexString: "#4DB4FF") ?? .black), .action { [weak self] in
            self?.privacyPolicy()
        })

        label.attributed.text = "\(wrap: str, .paragraph(.alignment(.center), .lineSpacing(5)))"
        label.lineBreakMode = .byWordWrapping
        
        return label
    }()
}
