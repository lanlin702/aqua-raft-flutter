//
//  ARRecommendationPostReviewAlert.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/25.
//

import UIKit
import AttributedString
import SnapKit

class ARRecommendationPostReviewAlert: ARBaseModalAlertController {
    
    var dismissCallback: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.baseContainerView.addSubview(containerBgView)
        containerBgView.addSubview(contentLabel)
        containerBgView.addSubview(sureBtn)
        
        containerBgView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.equalTo(315)
            make.height.equalTo(249)
            make.edges.equalToSuperview()
        }
        
        contentLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(48)
            make.left.equalToSuperview().offset(24)
            make.right.equalToSuperview().offset(-24)
        }
        
        sureBtn.snp.makeConstraints { make in
            make.width.equalTo(180)
            make.height.equalTo(40)
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-40)
        }
        
        self.autoDismiss = false
    }
    
    @objc private func sureBtnClick(_ sender: UIButton) {
        let callback = dismissCallback
        self.dismiss(animated: true) {
            callback?()
        }
    }

    // MARK: - Lazy
    
    private lazy var containerBgView: UIImageView = {
        let view = UIImageView(image: .init(named: "recommendation_post_review_alert_bg"))
        view.isUserInteractionEnabled = true
        return view
    }()
    
    private lazy var contentLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        
        let str = ARHelper.localString("Your application has been received. We will review your information carefully and approve it in the shortest possible time.")
        
        let att = ASAttributedString.init(string: str, .font(ARHelper.font(14, weight: .regular)),
            .foreground(.init(hexString: "#404040") ?? .black))
        label.attributed.text = "\(wrap: att, .paragraph(.alignment(.center), .lineSpacing(5)))"
        
        return label
    }()
    
    private lazy var sureBtn: UIButton = {
        let btn = UIButton.ar.themeStyleButton(height: 40, font: ARHelper.font(16, weight: .regular))
        btn.addTarget(self, action: #selector(sureBtnClick(_:)), for: .touchUpInside)
        btn.setTitle(ARHelper.localString("OK"), for: .normal)
        return btn
    }()

}
