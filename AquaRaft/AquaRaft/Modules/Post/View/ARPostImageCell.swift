//
//  ARPostImageCell.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/11.
//

import UIKit
import SnapKit
import JKSwiftExtension

class ARPostImageCell: UICollectionViewCell {
    
    var deleteCallback: (() -> Void)?
    
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - UI
    
    private func setupUI() {
        self.contentView.backgroundColor = UIColor(hexString: "#F6F8FA")
        self.contentView.layer.cornerRadius = 8
        self.contentView.layer.masksToBounds = true
        
        self.contentView.addSubview(takePhotoIconView)
        self.contentView.addSubview(photoImageView)
        self.contentView.addSubview(deleteBtn)
        
        takePhotoIconView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(43)
            make.left.equalToSuperview().offset(36)
        }
        
        photoImageView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        deleteBtn.snp.makeConstraints { make in
            make.top.right.equalToSuperview()
            make.width.height.equalTo(24)
        }
    }
    
    // MARK: - Action
    
    /// 更新cell
    func updateCell(imagePath: String?, deleteStyleIsIndex: Bool) {
        
        // 关闭按钮 - 首页进来和其他进来的不一样
        
        if deleteStyleIsIndex {
            deleteBtn.setBackgroundImage(UIImage.jk.image(color: .black.withAlphaComponent(0.6)), for: .normal)
            deleteBtn.setImage(.init(named: "post_edit_img_delete_deleteStyle"), for: .normal)
        } else {
            deleteBtn.setImage(.init(named: "post_edit_img_delete_closeStyle"), for: .normal)
            deleteBtn.setBackgroundImage(nil, for: .normal)
        }
        
        if let imagePath = imagePath {
            takePhotoIconView.isHidden = true
            photoImageView.isHidden = false
            deleteBtn.isHidden = false
            photoImageView.ar.setImage(imagePath)
            return
        }
        
        takePhotoIconView.isHidden = false
        photoImageView.isHidden = true
        deleteBtn.isHidden = true
    }
    
    @objc private func onDeleteBtnClick(_ sender: UIButton) {
        deleteCallback?()
    }
    
    // MARK: - Lazy
    
    private lazy var takePhotoIconView: UIImageView = {
        let iconView = UIImageView(image: .init(named: "common_take_photo"))
        iconView.isHidden = true
        return iconView
    }()
    
    private lazy var photoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
        
    private lazy var deleteBtn: UIButton = {
        let btn = UIButton(type: .custom)
        btn.addTarget(self, action: #selector(onDeleteBtnClick(_:)), for: .touchUpInside)
        btn.isHidden = true
        btn.setBackgroundImage(UIImage.jk.image(color: .black.withAlphaComponent(0.6)), for: .normal)
        
        return btn
    }()
}
