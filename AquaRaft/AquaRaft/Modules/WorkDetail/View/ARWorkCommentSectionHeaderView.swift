//
//  ARWorkCommentSectionHeaderView.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/22.
//

import UIKit
import SnapKit

class ARWorkCommentSectionHeaderView: UITableViewHeaderFooterView {

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        self.contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(16)
            make.top.equalToSuperview().offset(24)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .init(hexString: "#202020")
        label.font = ARHelper.font(16, weight: .medium)
        return label
    }()
}
