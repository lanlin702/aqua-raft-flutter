//
//  ARWorkContentWithTranslateView.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/22.
//

import UIKit
import SnapKit
import AttributedString

class ARWorkContentWithTranslateView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        self.addSubview(workContentLabel)
        self.addSubview(workTranslateContentLabel)
        self.addSubview(separatorLine)
        
        workContentLabel.snp.makeConstraints { make in
            make.left.right.top.equalToSuperview()
        }
        
        workTranslateContentLabel.snp.makeConstraints { make in
            make.left.right.bottom.equalToSuperview()
            make.top.equalTo(separatorLine.snp.bottom).offset(0)
        }
        
        separatorLine.snp.makeConstraints { make in
            make.left.right.equalToSuperview()
            make.top.equalTo(workContentLabel.snp.bottom).offset(0)
            make.height.equalTo(1)
        }
    }
    
    /// 更新内容
    func update(content: String, translateContent: String?) {
        let contentAttri = ASAttributedString(string: content,
                        .font(ARHelper.font(14, weight: .regular)),
                        .foreground(UIColor(hexString: "#565656") ?? .black),
                        .paragraph(.alignment(.left), .minimumLineHeight(21), .maximumLineHeight(21)))
        workContentLabel.attributed.text = contentAttri
        
        if let translateContent = translateContent {
            
            let translateContentAttri = ASAttributedString(string: translateContent,
                            .font(ARHelper.font(14, weight: .regular)),
                            .foreground(UIColor(hexString: "#565656") ?? .black),
                            .paragraph(.alignment(.left), .minimumLineHeight(21), .maximumLineHeight(21)))
            
            workTranslateContentLabel.attributed.text = translateContentAttri
            
            separatorLine.isHidden = false
            workTranslateContentLabel.isHidden = false
            workTranslateContentLabel.snp.updateConstraints { make in
                make.top.equalTo(separatorLine.snp.bottom).offset(8)
            }
            separatorLine.snp.updateConstraints { make in
                make.top.equalTo(workContentLabel.snp.bottom).offset(8)
            }
            
        } else {
            workTranslateContentLabel.attributed.text = nil
            separatorLine.isHidden = true
            workTranslateContentLabel.isHidden = true
            workTranslateContentLabel.snp.updateConstraints { make in
                make.top.equalTo(separatorLine.snp.bottom).offset(0)
            }
            separatorLine.snp.updateConstraints { make in
                make.top.equalTo(workContentLabel.snp.bottom).offset(0)
            }
        }
    }
    
    private lazy var separatorLine: UIView = {
        let view = UIView()
        view.backgroundColor = .init(hexString: "#565656")
        view.isHidden = true
        return view
    }()
    
    private lazy var workContentLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var workTranslateContentLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
}
