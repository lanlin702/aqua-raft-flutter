//
//  ARVideoWorkCommentCell.h
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/21.
//

#import <UIKit/UIKit.h>
#import "ARVideoWorkCommentCellData.h"

NS_ASSUME_NONNULL_BEGIN

@interface ARVideoWorkCommentCell : UITableViewCell

@property (nonatomic, strong) ARVideoWorkCommentCellData *cellData;

@end

NS_ASSUME_NONNULL_END
