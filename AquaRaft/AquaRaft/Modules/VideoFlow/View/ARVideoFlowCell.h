//
//  ARVideoFlowCell.h
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/21.
//

#import <UIKit/UIKit.h>
#import "ARPlayerView.h"

@class ARVideoWorkModel, ARVideoFlowCell;

@protocol ARVideoFlowCellDelegate <NSObject>

@optional

- (void)videoFlowCellDidClickGiftBtn:(ARVideoFlowCell *)cell;
- (void)videoFlowCellDidClickLikeBtn:(ARVideoFlowCell *)cell;
- (void)videoFlowCellDidClickCommentBtn:(ARVideoFlowCell *)cell;
- (void)videoFlowCellDidClickFollowBtn:(ARVideoFlowCell *)cell;

@end

typedef void (^OnPlayerReady)(void);

@interface ARVideoFlowCell : UITableViewCell

@property (nonatomic, strong) OnPlayerReady    onPlayerReady;
@property (nonatomic, assign) BOOL isPlayerReady;

@property (nonatomic, strong) ARVideoWorkModel *model;

@property (nonatomic, weak) id<ARVideoFlowCellDelegate> delegate;

@property (nonatomic, strong, readonly) ARPlayerView *playerView;

#pragma mark - Video control Function

- (void)play;
- (void)pause;
- (void)replay;
- (void)startDownloadBackgroundTask;
- (void)startDownloadHighPriorityTask;

@end
