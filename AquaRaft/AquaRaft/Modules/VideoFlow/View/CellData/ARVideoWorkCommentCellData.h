//
//  ARVideoWorkCommentCellData.h
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/21.
//

#import <Foundation/Foundation.h>

@class ARCommentModel;

extern CGFloat kVideoWrokCommentAvatarWidth;
extern CGFloat kVideoWrokCommentAvatarPadding;
/// containerview 与边框的padding
extern CGFloat kVideoWrokCommentCellContainerHoriPadding;
/// 底部的padding
extern CGFloat kVideoWrokCommentCellContainerBottomPadding;
/// 文字内容的左右边距
extern CGFloat kVideoWorkCommentStringHoriPadding;
/// 文字内容的上下边距
extern CGFloat kVideoWorkCommentStringVeriPadding;


@interface ARVideoWorkCommentCellData : NSObject

@property (nonatomic, strong, readonly) ARCommentModel *commentModel;
@property (nonatomic, strong, readonly) NSAttributedString *commentText;

@property (nonatomic, assign, readonly) CGFloat cellHeight;
@property (nonatomic, assign, readonly) CGSize commentTextSize;

/// cell宽度 - 决定tableView的宽度
+ (CGFloat)cellWidth;

+ (instancetype)cellDataWithCommentModel:(ARCommentModel *)model;

- (instancetype)init __unavailable;



@end
