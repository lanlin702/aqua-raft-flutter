//
//  ARVideoWorkCommentCellData.m
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/21.
//

#import "ARVideoWorkCommentCellData.h"
#import <UIKit/UIKit.h>

#import "AquaRaft-Swift.h"

CGFloat kVideoWrokCommentAvatarWidth = 32.0;
CGFloat kVideoWrokCommentAvatarPadding = 4.0;
/// containerview 与边框的padding
CGFloat kVideoWrokCommentCellContainerHoriPadding = 20.0;
/// 底部的padding
CGFloat kVideoWrokCommentCellContainerBottomPadding = 36.0;
/// 文字内容的左右边距
CGFloat kVideoWorkCommentStringHoriPadding = 10.0;
/// 文字内容的上下边距
CGFloat kVideoWorkCommentStringVeriPadding = 10.0;

@interface ARVideoWorkCommentCellData ()

@property (nonatomic, strong) ARCommentModel *commentModel;

@property (nonatomic, strong) NSAttributedString *commentText;

@property (nonatomic, assign) CGFloat cellHeight;
@property (nonatomic, assign) CGSize commentTextSize;

@end

@implementation ARVideoWorkCommentCellData

+ (instancetype)cellDataWithCommentModel:(ARCommentModel *)model {
    ARVideoWorkCommentCellData *cellData = [ARVideoWorkCommentCellData new];
    [cellData configCellData:model];
    return cellData;
}

/// cell宽度 - 决定tableView的宽度
+ (CGFloat)cellWidth {
    return kVideoWrokCommentCellContainerHoriPadding * 2 + kVideoWrokCommentAvatarPadding + kVideoWrokCommentAvatarWidth + kVideoWorkCommentStringHoriPadding * 2 + [self commentMaxWidth];
}

+ (CGFloat)commentMaxWidth {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat maxWidth = screenWidth * (170.0 / 375.0);
    return maxWidth;
}

- (void)configCellData:(ARCommentModel *)model {
    _commentModel = model;
    // 主要是计算宽度与高度
    NSString *commentText = model.comment;
    if (!commentText) {
        commentText = @"";
    }
    
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    style.minimumLineHeight = 20;
    style.maximumLineHeight = 20;
    style.alignment = NSTextAlignmentLeft;
    
    self.commentText = [[NSAttributedString alloc] initWithString:commentText attributes:@{
        NSFontAttributeName: [UIFont systemFontOfSize:15 weight:UIFontWeightRegular],
        NSForegroundColorAttributeName: [UIColor whiteColor],
        NSParagraphStyleAttributeName : style
    }];
    
    CGFloat textMaxWidth = [ARVideoWorkCommentCellData commentMaxWidth];
    CGSize textSize = [self.commentText boundingRectWithSize:CGSizeMake(textMaxWidth, CGFLOAT_MAX) options:NSStringDrawingUsesFontLeading context:nil].size;
    if (textSize.height < 20) {
        textSize.height = 20;
    }
    self.commentTextSize = textSize;
    
    // 高度
    self.cellHeight = 0;
    self.cellHeight += kVideoWorkCommentStringVeriPadding * 2 + textSize.height + kVideoWrokCommentCellContainerBottomPadding;
}

@end
