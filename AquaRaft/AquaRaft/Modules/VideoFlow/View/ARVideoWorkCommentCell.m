//
//  ARVideoWorkCommentCell.m
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/21.
//

#import "ARVideoWorkCommentCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <Masonry/Masonry.h>

#import "AquaRaft-Swift.h"

@interface ARVideoWorkCommentCell ()

@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) UIImageView *avatarView;
@property (nonatomic, strong) UILabel *commentTextLabel;

@end

@implementation ARVideoWorkCommentCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    
    [self.contentView addSubview:self.containerView];
    [self.containerView addSubview:self.avatarView];
    [self.containerView addSubview:self.commentTextLabel];
    
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(kVideoWrokCommentCellContainerHoriPadding);
        make.right.lessThanOrEqualTo(self.contentView).offset(-kVideoWrokCommentCellContainerHoriPadding);
        make.bottom.equalTo(self.contentView).offset(-kVideoWrokCommentCellContainerBottomPadding);
    }];
    
    [self.avatarView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(kVideoWrokCommentAvatarWidth);
        make.top.left.equalTo(self.containerView).offset(kVideoWrokCommentAvatarPadding);
    }];
    
    [self.commentTextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.avatarView.mas_right).offset(kVideoWorkCommentStringHoriPadding);
        make.right.equalTo(self.containerView).offset(-kVideoWorkCommentStringHoriPadding);
        make.top.equalTo(self.containerView).offset(kVideoWorkCommentStringVeriPadding);
        make.bottom.equalTo(self.containerView).offset(-kVideoWorkCommentStringVeriPadding);
    }];
}

- (void)setCellData:(ARVideoWorkCommentCellData *)cellData {
    _cellData = cellData;
    
    NSString *avatar = cellData.commentModel.author.trueAvatar;
    if (avatar != nil) {
        if ([avatar containsString:@"http"]) {
            [self.avatarView sd_setImageWithURL:[NSURL URLWithString:avatar]];
        } else {
            self.avatarView.image = [UIImage imageWithContentsOfFile:avatar];
        }
    } else {
        self.avatarView.image = nil;
    }
    
    self.commentTextLabel.attributedText = cellData.commentText;
}

#pragma mark - Getter

- (UIView *)containerView {
    if (!_containerView) {
        _containerView = [UIView new];
        _containerView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
        _containerView.layer.cornerRadius = 20.0;
        _containerView.layer.borderColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6].CGColor;
        _containerView.layer.borderWidth = 1.0;
    }
    return _containerView;
}

- (UIImageView *)avatarView {
    if (!_avatarView) {
        _avatarView = [UIImageView new];
        _avatarView.layer.cornerRadius = kVideoWrokCommentAvatarWidth * 0.5;
        _avatarView.layer.masksToBounds = YES;
        _avatarView.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.5];
    }
    return _avatarView;
}

- (UILabel *)commentTextLabel {
    if (!_commentTextLabel) {
        _commentTextLabel = [UILabel new];
        _commentTextLabel.numberOfLines = 0;
    }
    return _commentTextLabel;
}

@end
