//
//  ARReciveGiftView.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/22.
//

import UIKit
import SnapKit

/// 收到的礼物view
/// - note: 大小固定
class ARReciveGiftView: UIView {

    let userId: String

    init(userId: String) {
        self.userId = userId
        
        super.init(frame: .zero)
        
        self.setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        
        let size = intrinsicContentSize
        
        self.layer.cornerRadius = 8
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.1
        
        let gradientLayer = CAGradientLayer()
        // 设置渐变的颜色
        gradientLayer.colors = [UIColor.init(hexString: "#D9FFFD")!.cgColor, UIColor.init(hexString: "#D4F2FF")!.cgColor]
        // 设置渐变的开始和结束位置
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
        gradientLayer.frame = .init(x: 0, y: 0, width: size.width, height: size.height)
        gradientLayer.cornerRadius = 8
        gradientLayer.masksToBounds = true
        self.layer.addSublayer(gradientLayer)
        
        let boatView = UIImageView(image: .init(named: "gift_boat_small"))
        let boatLabel = UILabel()
        boatLabel.textColor = UIColor(hexString: "#0B4977")
        boatLabel.font = ARHelper.font(18, weight: .medium)
        boatLabel.text = String(ARDataCenter.default.boatGiftNumOf(userId))
        boatLabel.textAlignment = .center
        self.addSubview(boatView)
        self.addSubview(boatLabel)
        boatView.snp.makeConstraints { make in
            make.width.equalTo(61)
            make.height.equalTo(64)
            make.left.equalToSuperview().offset(ARHelper.widthScale(20))
            make.bottom.equalToSuperview().offset(-26)
        }
        boatLabel.snp.makeConstraints { make in
            make.left.right.equalTo(boatView)
            make.top.equalTo(boatView.snp.bottom)
            make.bottom.equalToSuperview()
        }
        
        let waterGunView = UIImageView(image: .init(named: "gift_waterGun_small"))
        let waterGunLabel = UILabel()
        waterGunLabel.textColor = UIColor(hexString: "#0B4977")
        waterGunLabel.font = ARHelper.font(18, weight: .medium)
        waterGunLabel.text = String(ARDataCenter.default.waterGunGiftNumOf(userId))
        waterGunLabel.textAlignment = .center
        self.addSubview(waterGunView)
        self.addSubview(waterGunLabel)
        waterGunView.snp.makeConstraints { make in
            make.width.equalTo(70)
            make.height.equalTo(56)
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-26)
        }
        waterGunLabel.snp.makeConstraints { make in
            make.left.right.equalTo(waterGunView)
            make.top.equalTo(waterGunView.snp.bottom)
            make.bottom.equalToSuperview()
        }
        
        let swimmingRingView = UIImageView(image: .init(named: "gift_swimmingRing_small"))
        let swimmingRingLabel = UILabel()
        swimmingRingLabel.textColor = UIColor(hexString: "#0B4977")
        swimmingRingLabel.font = ARHelper.font(18, weight: .medium)
        swimmingRingLabel.text = String(ARDataCenter.default.swimmingRingGiftNumOf(userId))
        swimmingRingLabel.textAlignment = .center
        self.addSubview(swimmingRingView)
        self.addSubview(swimmingRingLabel)
        swimmingRingView.snp.makeConstraints { make in
            make.width.equalTo(71)
            make.height.equalTo(60)
            make.right.equalToSuperview().offset(ARHelper.widthScale(-20))
            make.bottom.equalToSuperview().offset(-26)
        }
        swimmingRingLabel.snp.makeConstraints { make in
            make.left.right.equalTo(swimmingRingView)
            make.top.equalTo(swimmingRingView.snp.bottom)
            make.bottom.equalToSuperview()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        let width = ARHelper.screenWidth() - 2 * 16
        return .init(width: width , height: 98.0)
    }
}
