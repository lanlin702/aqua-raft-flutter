//
//  ARGiftModel.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/20.
//

import Foundation

struct ARGiftModel {
    
    static let boatGiftId = "ar.gift.boat_id"
    static let waterGunGiftId = "ar.gift.waterGun_id"
    static let swimmingRingGiftId = "ar.gift.swimmingRing_id"
    
    let icon: String
    let animate: String
    
    let id: String
    let coins: Int // 需要的coin
    
    init(icon: String, animate: String, id: String, coins: Int) {
        self.icon = icon
        self.animate = animate
        self.id = id
        self.coins = coins
    }
    
    static func boat() -> ARGiftModel {
        return ARGiftModel(icon: "gift_boat_small", animate: "gift_boat_animate", id: Self.boatGiftId, coins: 48)
    }
    
    static func waterGun() -> ARGiftModel {
        return ARGiftModel(icon: "gift_waterGun_small", animate: "gift_waterGun_animate", id: Self.waterGunGiftId, coins: 48)
    }
    
    static func swimmingRing() -> ARGiftModel {
        return ARGiftModel(icon: "gift_swimmingRing_small", animate: "gift_swimmingRing_animate", id: Self.swimmingRingGiftId, coins: 48)
    }
}
