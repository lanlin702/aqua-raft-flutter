//
//  ARGiftPurchaseApi.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/23.
//

import Foundation
import Alamofire

class ARGiftPurchaseRequest: ARRequest {
    
    override var api: String {
        "/coin/reviewModeConsume"
    }
    
    override var httpMethod: HTTPMethod {
        .post
    }
    
    func purchageGift(_ coins: Int, completion: @escaping (Error?) -> Void) {
        self.parameters = [
            "outlay" : coins,
            "source" : "iap"
        ]
        self.request { response, result in
            switch result {
            case .faild(let err):
                completion(err)
            case .successWithoutResponse, .success(_):
                completion(nil)
            }
        }
    }
    
}
