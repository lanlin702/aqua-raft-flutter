//
//  ARAppLauncher.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/3/24.
//

import Foundation
import UIKit
import FDFullscreenPopGesture

class ARAppLauncher {
    
    // MARK: - Public property
    
    // 是否第一次安装
    let isFirstInstall: Bool
    
    private static let firstInstallKey = "ARAppLauncher.firstInstallKey"
    
    // MARK: - 单例
    
    static let share = ARAppLauncher()
    
    private init() {
        self.isFirstInstall = UserDefaults.standard.object(forKey: Self.firstInstallKey) != nil ? false : true
        // 记录
        UserDefaults.standard.set("Installed", forKey: Self.firstInstallKey)
        UserDefaults.standard.synchronize()
        
        self.configManager()
    }
    
    private func configManager() {
        
    }
    
    // MARK: - Public
    
    /// 跳转至登录页 -> 直接修改window.rootController
    func transitionToLoginPage() {
        DispatchQueue.main.async {
            let loginVc = ARLoginViewController()
            let nav = ARNavigationController(rootViewController: loginVc)
            loginVc.fd_prefersNavigationBarHidden = true
            
            if let optionWindow =  UIApplication.shared.delegate?.window, let window = optionWindow {
                window.rootViewController = nav
                window.makeKeyAndVisible()
            }
        }
    }
    
    /// 跳转至首页
    func transitionToIndexPage() {
        DispatchQueue.main.async {
            let tabbar = ARTabbarController(nibName: nil, bundle: nil)
            if let optionWindow =  UIApplication.shared.delegate?.window, let window = optionWindow {
                window.rootViewController = tabbar
                window.makeKeyAndVisible()
            }
        }
    }
}
