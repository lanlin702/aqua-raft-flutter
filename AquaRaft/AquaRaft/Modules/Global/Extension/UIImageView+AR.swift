//
//  UIImageView+AR.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/8.
//

import UIKit
import Kingfisher
import JKSwiftExtension

//extension UIImageView: ARPOPCompatible {}

extension ARPOP where Base: UIImageView {
    
    func setImage(_ path: String) {
        // 判断路径
        if path.jk.contains(find: "http") {
            // 网络图片
            let url = URL(string: path)
            self.base.kf.setImage(with: url)
            return
        }
        if path.jk.contains(find: "/") {
            // 本地路径
            self.base.image = .init(contentsOfFile: path)
            return
        }
        
        // 普通名字
        self.base.image = .init(named: path)
    }
    
}

