//
//  UIButton+AR.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/20.
//

import UIKit
import Kingfisher
import JKSwiftExtension

extension ARPOP where Base: UIButton {
    
    /// 创建一个蓝色渐变按钮
    static func themeStyleButton(height: CGFloat = 48, font: UIFont = UIFont.systemFont(ofSize: 16, weight: .medium)) -> UIButton {
        let btn = UIButton(type: .custom)
        btn.titleLabel?.font = font
        btn.setBackgroundImage(UIImage.jk.gradient(["#39BAE8", "#5892E9"], size: .init(width: 1, height: height), locations: nil, direction: .vertical), for: .normal)
        btn.layer.cornerRadius = height * 0.5
        btn.layer.masksToBounds = true
        return btn
    }
    
}
