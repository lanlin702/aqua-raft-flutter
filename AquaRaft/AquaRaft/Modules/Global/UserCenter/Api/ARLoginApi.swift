//
//  ARLoginApi.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/3/31.
//

import Foundation
import Alamofire

class ARAutoLoginRequest: ARRequest {
    
    override var api: String {
        "/security/isValidToken"
    }
    
    override var httpMethod: HTTPMethod {
        .post
    }
    
    override var encoder: ParameterEncoding {
        JSONEncoding.default
    }
    
    func autoLogin(_ token: String, completion: @escaping (_ data: AnyDecodable?, _ err: ARRequestError?) -> Void) {
        self.parameters = [
            "token" : token
        ]
        self.request { response, result in
            switch result {
            case .faild(let err):
                completion(nil, err)
            case .successWithoutResponse, .success(_):
                completion(nil, nil)
            }
        }
    }
}

class ARLoginRequest: ARRequest {
    
    enum loginType: Int {
        case apple = 3
        case fast = 4
    }
    
    override var api: String {
        "/security/oauth"
    }
    
    override var httpMethod: HTTPMethod {
        .post
    }
    
    override var encoder: ParameterEncoding {
        URLEncoding.default
    }
    
    func login(_ type: loginType, token: String,completion: @escaping (ARRequestResult<ARLoginResponse>) -> Void) {
        self.parameters = [
            "oauthType" : type.rawValue,
            "token" : token
        ]
        self.request(of: ARLoginResponse.self) { response, result in
            completion(result)
        }
    }
}

class ARLogoutRequest: ARRequest {
    
    override var api: String {
        "/security/logout"
    }
    
    override var httpMethod: HTTPMethod {
        .post
    }
    
    func logout(_ completion: @escaping (ARRequestResult<AnyDecodable>) -> Void) {
        self.request { response, result in
            completion(result)
        }
    }
    
}

class ARDeleteAccountRequest: ARRequest {
    
    override var api: String {
        "/user/deleteAccount"
    }
    
    override var httpMethod: HTTPMethod {
        .post
    }
    
    func delete(_ completion: @escaping (ARRequestResult<AnyDecodable>) -> Void) {
        self.request { response, result in
            completion(result)
        }
    }
    
}
