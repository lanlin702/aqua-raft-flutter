//
//  ARFetchUserInfoRequest.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/3.
//

import UIKit

class ARFetchUserInfoRequest: ARRequest {
    override var api: String {
        "/user/getUserInfo"
    }
    
    func fetchUserInfo(_ userId: String, completion: @escaping (ARUserModel?, ARRequestError?) -> Void) {
        self.parameters = [
            "userId" : userId,
        ]
        self.request(of: ARUserModel.self) { response, result in
            switch result {
            case .faild(let err):
                completion(nil, err)
                break
            case .success(let model):
                completion(model, nil)
                break
            case .successWithoutResponse:
                // 不会出现这种情况
                completion(nil, .modelConvertError(error: nil))
                break
            }
        }
    }
}
