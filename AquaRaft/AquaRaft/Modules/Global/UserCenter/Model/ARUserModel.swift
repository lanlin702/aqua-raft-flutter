//
//  ARUserModel.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/3/31.
//

import Foundation

/// 用户模型
@objcMembers
class ARUserModel: NSObject, Codable {
    /*
     {
         "aaaa": 0,
         "isFirstRegister": false,
         "token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxNzc0NDc2NDQxNTE0Mjc4OTEyIiwidXNlcl90eXBlIjoxLCJleHAiOjQ4Njc2NTM4NzcsImNyZWF0ZWQiOjE3MTE5ODAyNzc1MzV9.loiRLucCH1AibOGauXrkd1rO9ASdh35QGNFoyUUnNzRoF0Zc1IPhokUMhzC42nsxnDXHjkfkfqx0Pkxhsy53jw",
         "userInfo": {
           "age": 29,
           "auditStatus": 1,
           "availableCoins": 0,
           "avatar": "avatar/default/male_3.png",
           "avatarMiddleThumbUrl": "http://test-agilecdn.livchat.me/avatar/default/male_3.png?Expires=1716899909&OSSAccessKeyId=LTAI5tNPNT6g1inCHkxgcUGz&Signature=qTvU1fU9YHxZ%2FdZT%2B8g68OUpBt0%3D&x-oss-process=image%2Fresize%2Cm_lfit%2Ch_480%2Cw_480%2Climit_1",
           "avatarThumbUrl": "http://test-agilecdn.livchat.me/avatar/default/male_3.png?Expires=1716899909&OSSAccessKeyId=LTAI5tNPNT6g1inCHkxgcUGz&Signature=Xi%2F5ZNZXayCcgYj0nTID6aWLijM%3D&x-oss-process=image%2Fresize%2Cm_lfit%2Ch_160%2Cw_160%2Climit_1",
           "avatarUrl": "http://test-agilecdn.livchat.me/avatar/default/male_3.png?Expires=1716899909&OSSAccessKeyId=LTAI5tNPNT6g1inCHkxgcUGz&Signature=M%2FUmlVYOkfbhhKbxAnUGmdtoELY%3D&x-oss-process=image%2Fresize%2Cm_lfit%2Ch_1080%2Cw_1080%2Climit_1",
           "birthday": "1995-01-01",
           "country": "USA",
           "createTime": 1711903126439,
           "followNum": 0,
           "gender": 1,
           "giftWallAction": 0,
           "hasEquity": false,
           "isAnswer": true,
           "isBlock": false,
           "isHavePassword": false,
           "isInternal": false,
           "isMultiple": false,
           "isRecharge": false,
           "isReview": false,
           "isSwitchNotDisturbCall": false,
           "isSwitchNotDisturbIm": false,
           "isVip": false,
           "level": 0,
           "loginPkgName": "test.cusmofusion.ios",
           "nickname": "Augus",
           "praiseNum": 0,
           "registerCountry": "CN",
           "registerPkgName": "test.cusmofusion.ios",
           "rongcloudToken": "8yxTFD6CZQb+Fl8Se4hFCUZyDVlxst5EnL9go1cwpVBqo/MAErm/HsVaRr5Fi2jp@hw3r.cn.rongnav.com;hw3r.cn.rongcfg.com",
           "tagDetails": [
             {
               "tag": "NEW",
               "tagColor": "#24c45d",
               "tagTip": "Attention: If the call duration is < 40s, you'll not get coins."
             },
             {
               "tag": "Coinless",
               "tagColor": "#999999",
               "tagTip": "Send him a recharge link with discount after the call"
             },
             {
               "tag": "English",
               "tagColor": "#1278f4",
               "tagTip": "He speaks English. Talk to him if you can"
             }
           ],
           "tagsList": [
             "New"
           ],
           "userId": "1774476441514278912",
           "userType": 1
         }
       }
     
     */
    /*
     "userId": "1739995714274066432",
     "nickname": "44444999",
     "avatar": "http://test-agilecdn.livchat.me/livchat/images/dev/20230920/1695191251149.jpg?Expires=1716459193&OSSAccessKeyId=LTAI5tNPNT6g1inCHkxgcUGz&Signature=chtM1spsmO0BQbV2aUsNKjkRVwc%3D&x-oss-process=image%2Fresize%2Cm_lfit%2Ch_480%2Cw_480%2Climit_1",
     "avatarMapPath": "livchat/images/dev/20230920/1695191251149.jpg",
     "gender": 2,
     "age": 26,
     "country": "USA",
     "status": "Online",
     "callCoins": 100,
     "unit": "min",
     "videoMapPaths": [],
     "followNum": 1,
     "isFriend": false,
     "isMultiple": false,
     "analysisLanguage": "zh",
     "isSignBroadcaster": false,
     "showRoomVersion": 1
     */
    var userId: String
    
    var age: Int
    var availableCoins: Int?
    
    var birthday: String?
    var country: String
    var gender: Int
    var nickname: String
    
//    var auditStatus: Int
    var followNum: Int
    var isBlock: Bool?

    var isFollow: Bool? = false
    
    var avatarUrl: String?
    var avatar: String?
    
    var trueAvatar: String {
        get {
            if let avatarUrl = avatarUrl {
                return avatarUrl
            }
            if let avatar = avatar {
                return avatar
            }
            // 无效的头像
            fatalError("Invalid avatar")
        }
    }
    
    @objc func follow() -> Bool {
        return isFollow ?? false
    }
}

/// 登录返回
class ARLoginResponse: Decodable {
    
    var token: String
    var userInfo: ARUserModel
    
}
