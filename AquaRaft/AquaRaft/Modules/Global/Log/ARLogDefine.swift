//
//  ARLogDefine.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/5/4.
//

import Foundation

enum ARLogPage: String {
    
    // 启动页面
    case launch
    // 登录页面
    case quicklogin, applelogin, terms, privacy, loginsuccess
    // 内购
    case purchase, createorder
    // 我的页面
    case editavatar, logout, deleteaccount,customer
    // 其他
    case report, block
}
