//
//  ARLogStruct.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/5/3.
//

import Foundation

/// 上报的事件类型
struct ARLogTypeStruct {
    
    var logType: String
    var subType: String
    var behavior: String
    
    static let globalBehavior = ARLogTypeStruct(logType: "event", subType: "global_behavior", behavior: "event")
    static let clickEvennt = ARLogTypeStruct(logType: "event", subType: "click", behavior: "event")
    
    private init(logType: String, subType: String, behavior: String) {
        self.logType = logType
        self.subType = subType
        self.behavior = behavior
    }
}

/// 日志事件
struct ARLogStruct: Encodable {
    
    /// 具体日志事件
    var data: [AnyDecodable]
    
    var log_type: String
    var subtype: String
    var behavior: String
    
    /// launch_id
    var lan_id: String
    /// 日志的次数 - 从0递增
    var sec_id: Int
    
    var device_id: String = ARDevice.share.uuidResult.idfa ?? ""
    var user_id: String = ARUserManager.shared.userId ?? ""
    var pkg: String = ARHelper.bundleId()
    var chn_id: String = ""
    var ver: String = ARHelper.versionCode()
    var platform: String = "iOS"
    var model: String = ARDevice.share.phoneModel
    var p_ver: String = ARHelper.buildCode()
    
    var country: String = ARUserManager.shared.userModel?.country ?? ""
    var sys_lan: String = ARHelper.systemLanguageCode()
    
    /// af 归因信息
    var utm_source: String = ""
    var af_adgroup_id: String = ""
    var af_adset = ""
    var af_adset_id = ""
    var af_status = ""
    var af_agency = ""
    var af_channel = ""
    var campaign = ""
    var campaign_id = ""
    
    enum CodingKeys: String, CodingKey {
        case data, log_type, subtype, behavior, lan_id, sec_id
        case device_id = "device-id"
        case user_id, pkg, chn_id, ver, platform, model, p_ver, country, sys_lan
        case utm_source, af_adgroup_id, af_adset, af_adset_id, af_status, af_agency, af_channel, campaign, campaign_id
    }
}
