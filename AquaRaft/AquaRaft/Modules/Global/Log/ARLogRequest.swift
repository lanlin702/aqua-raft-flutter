//
//  ARLogRequest.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/5/3.
//

import UIKit
import Alamofire

/// 日志上报
class ARLogRequest: ARRequest {
    
    private var logList: [ARLogStruct] = []
    
    override var api: String {
        "/log/live-chat"
    }
    
    override var host: String {
        "http://log.cosmofusionly.xyz"
    }
    
    override var encoder: ParameterEncoding {
        let encoder = JSONEncoder()
        let data = try! encoder.encode(logList)
        return DataEncoding(data: data)
    }
    
    override var httpMethod: HTTPMethod {
        .post
    }
    
    func uploadLog(_ logList: [ARLogStruct], completion: @escaping (Error?) -> Void) {
        
        self.logList.removeAll()
        self.logList.append(contentsOf: logList)
        
        self.request { response, result in
            switch result {
            case .faild(let err):
                completion(err)
            case .successWithoutResponse, .success(_):
                completion(nil)
            }
        }
    }
    
}
