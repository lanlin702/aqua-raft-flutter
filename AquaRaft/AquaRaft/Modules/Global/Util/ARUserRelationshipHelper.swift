//
//  ARUserRelationshipHelper.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/21.
//

import Foundation
import UIKit

struct ARUserRelationshipHelper {
    
    // 操作的类型
    enum operationType {
        case follow, blackList
    }
    
    private init() {
        
    }
    
    /// 点击 more 按钮
    static func workMoreAction(user: ARUserModel, vc: UIViewController, completion: @escaping (operationType, Error?) -> Void) {
        
        let followStr = ARHelper.localString("Follow")
        let blockStr = ARHelper.localString("Block")
        let reportStr = ARHelper.localString("Report")
        
        let cancelAction = UIAlertAction(title: ARHelper.localString("Cancel"), style: .cancel)
        let followAction = UIAlertAction(title: followStr, style: .default) { [weak vc] _ in
            // 关注
            vc?.ar.showLoading()
            ARDataCenter.default.followOrUnfollowUser(user.userId, followOrUnfollow: true) { err in
                ARHelper.afterAsyncInMainQueue {
                    vc?.ar.hideHUD()
                    ARHelper.window().ar.makeToast(ARHelper.localString("Follow Successfully"))
                    completion(.follow, err)
                }
            }
        }
        let blockAction = UIAlertAction(title: blockStr, style: .default) { [weak vc] _ in
            // 拉黑
            ARLogManager.logPage(.block)
            
            vc?.ar.showLoading()
            ARDataCenter.default.addOrRemoveBlackList(user.userId, addOrRemove: true) { err in
                ARHelper.afterAsyncInMainQueue {
                    vc?.ar.hideHUD()
                    ARHelper.window().makeToast(ARHelper.localString("Block Successfully"))
                    completion(.blackList, err)
                }
            }
        }
        let reportAction = UIAlertAction(title: reportStr, style: .default) { [weak vc] _ in
            // 举报
            ARLogManager.logPage(.report)
            guard let vc = vc else {
                return
            }
            Self._reportAction(user: user, vc: vc, completion: completion)
        }
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(cancelAction)
        if user.isFollow ?? false == false {
            alert.addAction(followAction)
        }
        alert.addAction(blockAction)
        alert.addAction(reportAction)
        
        vc.present(alert, animated: true)
    }
    
    private static func _reportAction(user: ARUserModel, vc: UIViewController, completion: @escaping (operationType, Error?) -> Void) {
        let list = [
            ARHelper.localString("Pornographic"),
            ARHelper.localString("False gender"),
            ARHelper.localString("Fraud"),
            ARHelper.localString("Political sensitive"),
            ARHelper.localString("Other"),
        ]
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: ARHelper.localString("Cancel"), style: .cancel)
        
        alert.addAction(action: cancelAction)
        for str in list {
            let reportAction = UIAlertAction(title: str, style: .default) { [weak vc] _ in
                vc?.ar.showLoading()
                ARHelper.afterAsyncInMainQueue {
                    vc?.ar.hideHUD()
                    ARHelper.window().makeToast(ARHelper.localString("Report Successfully"))
//                    completion(.blackList, nil)
                }
//                ARDataCenter.default.addOrRemoveBlackList(user.userId, addOrRemove: true, subReason: str) { err in
//                    vc?.ar.hideHUD()
//                    completion(.blackList, err)
//                }
            }
            alert.addAction(reportAction)
        }
        vc.present(alert, animated: true)
    }
    
}
