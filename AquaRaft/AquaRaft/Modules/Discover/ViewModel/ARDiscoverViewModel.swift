//
//  ARDiscoverViewModel.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/21.
//

import Foundation

class ARDiscoverViewModel: ARWorkWaterFlowViewModelProvider {
    
    var cellMoreActionType: ARWorkCollectionViewCell.MoreActionType = .more
    
    private var _dataSource: [ARWorkModel] = []
    var dataSource: [ARWorkModel] {
        set {
            _dataSource = newValue
        }
        get {
            _dataSource
        }
    }
    
    func loadData(_ completion: @escaping (Error?) -> Void) {
        ARDataCenter.default.fetchWorkList { [weak self] workList, err in
            if let err = err {
                completion(err)
                return
            }
            guard let workList = workList else {
                completion(ARDataCenterError.unknown)
                return
            }
            self?._dataSource.removeAll()
            self?._dataSource.append(contentsOf: workList)
            completion(nil)
        }
    }
    
}
