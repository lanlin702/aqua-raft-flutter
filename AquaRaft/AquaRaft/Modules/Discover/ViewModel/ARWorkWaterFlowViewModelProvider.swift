//
//  ARWorkWaterFlowViewModelProvider.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/21.
//

import Foundation

protocol ARWorkWaterFlowViewModelProvider: AnyObject {
    
    /// dataSource
    var dataSource: [ARWorkModel] { get set }
    
    var cellMoreActionType: ARWorkCollectionViewCell.MoreActionType { get }
    
    /// 刷新方法
    func loadData(_ completion: @escaping (Error?) -> Void)
    
    func likeOrUnLikeWork(_ work: ARWorkModel, completion: @escaping (Error?) -> Void)
    func deleteWork(_ work: ARWorkModel, completion: @escaping (Error?) -> Void)
}

extension ARWorkWaterFlowViewModelProvider {
    
    func likeOrUnLikeWork(_ work: ARWorkModel, completion: @escaping (Error?) -> Void) {
        ARDataCenter.default.likeOrUnlikeWork(work.id, like: !(work.isLike ?? false)) { err in
            completion(err)
        }
    }
    
    func deleteWork(_ work: ARWorkModel, completion: @escaping (Error?) -> Void) {
        ARDataCenter.default.deleteWork(work.id) { [weak self] err in
            ARHelper.afterAsyncInMainQueue {
                
                if err == nil {
                    // 成功
                    self?.dataSource.removeAll(where: { $0.id == work.id})
                }
                
                completion(err)
            }
        }
    }
    
}
