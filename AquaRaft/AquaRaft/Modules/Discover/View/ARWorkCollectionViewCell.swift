//
//  ARWorkCollectionViewCell.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/21.
//

import UIKit
import SnapKit

@objc protocol ARWorkCollectionViewCellDelegate {
    
    @objc optional func workCollectionViewCellClickedLikeBtn(_ cell: ARWorkCollectionViewCell)
    @objc optional func workCollectionViewCellClickedAvatarBtn(_ cell: ARWorkCollectionViewCell)
    @objc optional func workCollectionViewCellClickedMoreBtn(_ cell: ARWorkCollectionViewCell, type: ARWorkCollectionViewCell.MoreActionType)
}

/// 作品cell
class ARWorkCollectionViewCell: UICollectionViewCell {
    
    // MARK: Define
    
    /// 更多按钮的样式
    @objc enum MoreActionType: Int {
        case more = 0
        case delete = 1
    }
    
    // MARK: - Property
    
    var model: ARWorkModel?
    var moreActionType: MoreActionType = .more
    
    /// Become effective only upon update model value
    var shouldHideMoreBtnWhenWorkAuthorIsCurrentUser = true
    
    weak var delegate: ARWorkCollectionViewCellDelegate?
    
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - UI
    
    private func setupUI() {
        self.contentView.addSubview(coverImageView)
        self.contentView.addSubview(avatarBtn)
        self.contentView.addSubview(nicknameLabel)
        self.contentView.addSubview(likeBtn)
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(moreBtn)
        
        self.layer.cornerRadius = 12
        self.layer.masksToBounds = true
        
        coverImageView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        avatarBtn.snp.makeConstraints { make in
            make.width.height.equalTo(20)
            make.left.equalToSuperview().offset(8)
            make.bottom.equalToSuperview().offset(-8)
        }
        
        nicknameLabel.snp.makeConstraints { make in
            make.left.equalTo(avatarBtn.snp.right).offset(8)
            make.right.equalTo(likeBtn.snp.left).offset(-8)
            make.centerY.equalTo(avatarBtn)
        }
        
        likeBtn.snp.makeConstraints { make in
            make.width.height.equalTo(16)
            make.right.equalToSuperview().offset(-8)
            make.centerY.equalTo(avatarBtn)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(8)
            make.right.equalToSuperview().offset(-8)
            make.bottom.equalTo(avatarBtn.snp.top).offset(-8)
        }
        
        moreBtn.snp.makeConstraints { make in
            make.width.height.equalTo(30)
            make.top.equalToSuperview().offset(8)
            make.right.equalToSuperview().offset(-8)
        }
    }
    
    // MARK: - Action
    
    @objc private func avatarBtnClicked(_ sender: UIButton) {
        delegate?.workCollectionViewCellClickedAvatarBtn?(self)
    }
    
    @objc private func moreBtnClicked(_ sender: UIButton) {
        delegate?.workCollectionViewCellClickedMoreBtn?(self, type: moreActionType)
    }
    
    @objc private func likeBtnClicked(_ sender: UIButton) {
        delegate?.workCollectionViewCellClickedLikeBtn?(self)
    }
    
    func updateModel(_ model: ARWorkModel, moreActionType: MoreActionType) {
        self.model = model
        self.moreActionType = moreActionType
        
        if let path = model.realPicCover() {
            coverImageView.ar.setImage(path)
        }
        if let avatarUrl = model.author?.trueAvatar,
           let url = URL(string: avatarUrl) {
            avatarBtn.kf.setImage(with: url, for: .normal)
        }
        nicknameLabel.text = model.author?.nickname ?? ""
        titleLabel.text = model.workTitle
        likeBtn.isSelected = model.isLike ?? false
        
        if shouldHideMoreBtnWhenWorkAuthorIsCurrentUser {
            if model.author?.userId ?? "" == ARUserManager.shared.userId ?? "" {
                moreBtn.isHidden = true
            } else {
                moreBtn.isHidden = false
            }
        } else {
            moreBtn.isHidden = false
        }
        
        switch moreActionType {
        case .more:
            moreBtn.setImage(.init(named: "common_more"), for: .normal)
        case .delete:
            moreBtn.setImage(.init(named: "mine_work_delete"), for: .normal)
        }
    }
    
    // MARK: - Lazy
    
    private lazy var coverImageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        return view
    }()
    
    private lazy var avatarBtn: UIButton = {
        let btn = UIButton(type: .custom)
        btn.addTarget(self, action: #selector(avatarBtnClicked(_:)), for: .touchUpInside)
        btn.layer.cornerRadius = 10
        btn.layer.masksToBounds = true
        btn.layer.borderColor = UIColor.white.cgColor
        btn.layer.borderWidth = 1
        return btn
    }()
    
    private lazy var nicknameLabel: UILabel = {
        let nameLabel = UILabel()
        nameLabel.font = ARHelper.font(12, weight: .regular)
        nameLabel.textColor = .white
        return nameLabel
    }()
    
    private lazy var titleLabel: UILabel = {
        let nameLabel = UILabel()
        nameLabel.font = ARHelper.font(12, weight: .regular)
        nameLabel.textColor = .white
        return nameLabel
    }()
    
    private lazy var moreBtn: UIButton = {
        let btn = UIButton(type: .custom)
        btn.addTarget(self, action: #selector(moreBtnClicked(_:)), for: .touchUpInside)
        return btn
    }()
    
    private lazy var likeBtn: UIButton = {
        let btn = UIButton(type: .custom)
        btn.addTarget(self, action: #selector(likeBtnClicked(_:)), for: .touchUpInside)
        btn.setImage(.init(named: "common_like_no_bg_btn_normal"), for: .normal)
        btn.setImage(.init(named: "common_like_no_bg_btn_selected"), for: .selected)
        return btn
    }()
}
