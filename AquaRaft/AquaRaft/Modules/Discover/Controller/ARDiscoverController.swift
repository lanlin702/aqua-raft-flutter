//
//  ARDiscoverController.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/2.
//

import UIKit
import SnapKit

class ARDiscoverController: ARBaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        refreshData()
    }
    // MARK: - UI
    
    private func setupUI() {
        
        self.ar_setupNavBgImg()
        let btn = UIButton(type: .custom)
        btn.setImage(.init(named: "index_post_icon"), for: .normal)
        btn.addTarget(self, action: #selector(postBtnClicked(_:)), for: .touchUpInside)
        let rightBarItem = UIBarButtonItem(customView: btn)
        self.navigationItem.rightBarButtonItem = rightBarItem
        
        let label = UILabel()
        label.text = ARHelper.localString("Discovery")
        label.font = ARHelper.font(24, weight: .heavy)
        label.textColor = UIColor.white
        let leftItem = UIBarButtonItem(customView: label)
        self.navigationItem.leftBarButtonItem = leftItem
        
        self.addChild(waterFlowVc)
        self.view.addSubview(waterFlowVc.view)
        waterFlowVc.view.snp.makeConstraints { make in
            make.top.left.right.equalToSuperview()
            make.bottom.equalToSuperview().offset(-ARHelper.tabBarHeight())
        }
    }
    
    // MARK: - Action

    @objc private func postBtnClicked(_ sender: UIButton) {
        let vc = ARDiscoverPostViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }

    // MARK: -
    
    private lazy var waterFlowVc: ARWorkWaterFlowController = {
        let viewModel = ARDiscoverViewModel()
        let vc = ARWorkWaterFlowController(viewModel: viewModel)
        vc.setupHeaderRefresherEnable(true)
        return vc
    }()
}

extension ARDiscoverController: ARTabControllerRefresh {
    func refreshData() {
        self.waterFlowVc.loadData()
    }
}
