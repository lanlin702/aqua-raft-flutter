//
//  ARMineViewModel.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/17.
//

import Foundation

enum ARMineAction {
    case gifts(iconName: String, title: String)
    case recommendation(iconName: String, title: String)
    case articles(iconName: String, title: String)
    case follow(iconName: String, title: String)
    case blackList(iconName: String, title: String)
    
    /// 获取value
    func value() -> (String, String) {
        switch self {
        case .gifts(let iconName, let title):
            return (iconName, title)
        case .recommendation(let iconName, let title):
            return (iconName, title)
        case .articles(let iconName, let title):
            return (iconName, title)
        case .follow(let iconName, let title):
            return (iconName, title)
        case .blackList(let iconName, let title):
            return (iconName, title)
        }
    }
}

class ARMineViewModel {
    
    let dataSource: [ARMineAction] = [
        .gifts(iconName: "mine_gift_icon", title: ARHelper.localString("My Gifts")),
        .recommendation(iconName: "mine_recommendation_icon", title: ARHelper.localString("My Recommendation")),
        .articles(iconName: "mine_articles_icon", title: ARHelper.localString("My Articles")),
        .follow(iconName: "mine_follow_icon", title: ARHelper.localString("My Follow")),
        .blackList(iconName: "mine_blackList_icon", title: ARHelper.localString("Block List"))
    ]
    
}
