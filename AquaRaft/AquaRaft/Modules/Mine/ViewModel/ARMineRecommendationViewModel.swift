//
//  ARMineRecommendationViewModel.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/23.
//

import Foundation

class ARMineRecommendationViewModel {
    private var _dataSource: [ARWorkModel] = []
    var dataSource: [ARWorkModel] {
        _dataSource
    }
    
    let userId: String = ARUserManager.shared.userId ?? ""
    
    /// 获取workData
    func loadData(_ completion: @escaping (Error?) -> Void) {
        // 作品
        ARDataCenter.default.fetchLoginUserAllReviewRecommendationList { [weak self] workList, err in
            if let err = err {
                completion(err)
                return
            }
            guard let workList = workList else {
                completion(ARRequestError.modelConvertError(error: nil))
                return
            }
            
            ARHelper.afterAsyncInMainQueue {
                self?._dataSource.removeAll()
                self?._dataSource.append(contentsOf: workList)
                completion(nil)
            }
        }
    }
    
    func deleteWork(_ work: ARWorkModel, completion: @escaping (Error?) -> Void) {
        ARDataCenter.default.deleteLoginUserRecommendation(work.id) { [weak self] err in
            ARHelper.afterAsyncInMainQueue {
                
                if err == nil {
                    // 成功
                    self?._dataSource.removeAll(where: { $0.id == work.id})
                }
                
                completion(err)
            }
        }
    }
    
}
