//
//  ARMineArticlesViewModel.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/22.
//

import Foundation

class ARMineArticlesViewModel: ARWorkWaterFlowViewModelProvider {
    
    let userId: String
    
    var dataSource: [ARWorkModel] = []
    var cellMoreActionType: ARWorkCollectionViewCell.MoreActionType = .delete
    
    init(userId: String) {
        self.userId = userId
    }
    
    /// 获取workData
    func loadData(_ completion: @escaping (Error?) -> Void) {
        // 作品
        ARDataCenter.default.fetchUserAllWorkList(userId) { [weak self] workList, err in
            if let err = err {
                completion(err)
                return
            }
            guard let workList = workList else {
                completion(ARRequestError.modelConvertError(error: nil))
                return
            }
            
            ARHelper.afterAsyncInMainQueue {
                self?.dataSource.removeAll()
                self?.dataSource.append(contentsOf: workList)
                completion(nil)
            }
        }
    }
}
