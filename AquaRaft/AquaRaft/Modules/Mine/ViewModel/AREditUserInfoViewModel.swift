//
//  AREditUserInfoViewModel.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/18.
//

import Foundation

class AREditUserInfoViewModel {
    
    /// 原始数据
    private var defaultAvatarUrl: String
    private var defaultNickname: String
    private var defaultBirthday: String
    private var defaultCountry: String
    
    /// 编辑数据
    var avatarUrl: String
    var nickname: String
    var birthday: String
    var country: String
    
    private var uploadUtil: ARUploadUtil<ARUploadResponseModel>?
    
    // init
    
    init() {
        // setup value
        guard let user = ARUserManager.shared.userModel else {
            fatalError("User did not log in.")
        }
        
        /// 全都得有值
        defaultAvatarUrl = user.trueAvatar
        defaultNickname = user.nickname
        defaultBirthday = user.birthday!
        defaultCountry = user.country
        
        avatarUrl = defaultAvatarUrl
        nickname = defaultNickname
        birthday = defaultBirthday
        country = defaultCountry
    }
    
    // MARK: - Public
    
    func updateCountry(_ country: String) {
        self.country = country
    }
    
    func updateNickname(_ nickname: String) {
        self.nickname = nickname
    }
    
    func updateBirthday(_ birthday: String) {
        self.birthday = birthday
    }
    
    // 更新头像
    
    func updateAvatar(_ data: Data, completion: @escaping (Error?) -> Void) {
        uploadUtil = ARUploadUtilHelper.uploadJpg(data, completion: { [weak self] result in
            self?.uploadUtil = nil
            switch result {
            case .successWithoutResponse: break
            case .faild(let err):
                completion(err)
                break
            case .success(let model):
                // 成功 -> 直接更新
                ARUpdateAvatarRequest().updateAvatar(model) { result in
                    switch result {
                    case .successWithoutResponse: break; // 这里直接不处理
                    case .faild(let err):
                        completion(err)
                    case .success(let model):
                        // 更新地址
                        self?.avatarUrl = model.mediaUrl
                        // 发送通知
                        NotificationCenter.default.post(name: .shouldUpdateUserInfo, object: nil)
                        completion(nil)
                    }
                }
                break
            }
        })
    }
    
    func updateInfoToRemote(_ completion: @escaping (Error?) -> Void) {
        ARUpdateUserInfoRequest().updateUserInfo(birthday: birthday, country: country, nickname: nickname) { [weak self] err in
            guard let self = self else {
                return
            }
            if err == nil {
                // 成功 -> 更新
                self.defaultCountry = self.country
                self.defaultBirthday = self.birthday
                self.defaultNickname = self.nickname
                
                // 发送通知
                NotificationCenter.default.post(name: .shouldUpdateUserInfo, object: nil)
            }
            completion(err)
        }
    }
    
    // MARK: Helper
    
    /// 数据是否有修改
    func valueDidChange() -> Bool {
        return (defaultNickname != nickname ||
                defaultCountry != country ||
                defaultBirthday != birthday)
    }
}
