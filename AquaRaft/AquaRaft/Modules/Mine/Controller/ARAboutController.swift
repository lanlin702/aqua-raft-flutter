//
//  ARAboutController.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/17.
//

import UIKit
import SnapKit
import JKSwiftExtension

/// 关于
class ARAboutController: ARBaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    private func setupUI() {
        
        // nav
        self.ar_setupNavBgImg()
        self.ar_configWhiteBackItem()
        self.title = ARHelper.localString("About")
        if let navigationBar = self.navigationController?.navigationBar {
            navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: ARHelper.font(18, weight: .medium)]
        }
        
        // icon
        let logo = UIImage(contentsOfFile: Bundle.main.path(forResource: "logo.png", ofType: nil)!)
        let logoView = UIImageView(image: logo)
        
        let nameLabel = UILabel()
        nameLabel.font = ARHelper.font(16, weight: .regular)
        nameLabel.textColor = .init(hexString: "#404040")
        nameLabel.text = ARHelper.appName()
        
        let versionLabel = UILabel()
        versionLabel.font = ARHelper.font(12, weight: .regular)
        versionLabel.textColor = .init(hexString: "#1493BC")
        versionLabel.text = ARHelper.localString("Version") + " \(ARHelper.versionCode())"
        
        let termsBtn = UIButton(type: .custom)
        termsBtn.titleEdgeInsets = .init(top: 0, left: 14, bottom: 0, right: 0)
        termsBtn.contentHorizontalAlignment = .left
        termsBtn.titleLabel?.font = ARHelper.font(16, weight: .regular)
        termsBtn.setTitleColor(.init(hexString: "#404040"), for: .normal)
        termsBtn.setTitle(ARHelper.localString("Terms and Conditons"), for: .normal)
        termsBtn.addTarget(self, action: #selector(termsBtnClicked(_:)), for: .touchUpInside)
        termsBtn.backgroundColor = (UIColor(hexString: "#F6F8FA") ?? .gray)
        termsBtn.layer.cornerRadius = 8
        
        let privacyPolicyBtn = UIButton(type: .custom)
        privacyPolicyBtn.titleEdgeInsets = .init(top: 0, left: 14, bottom: 0, right: 0)
        privacyPolicyBtn.contentHorizontalAlignment = .left
        privacyPolicyBtn.titleLabel?.font = ARHelper.font(16, weight: .regular)
        privacyPolicyBtn.setTitleColor(.init(hexString: "#404040"), for: .normal)
        privacyPolicyBtn.setTitle(ARHelper.localString("Privacy Policy"), for: .normal)
        privacyPolicyBtn.addTarget(self, action: #selector(privacyPolicyBtnClicked(_:)), for: .touchUpInside)
        privacyPolicyBtn.backgroundColor = (UIColor(hexString: "#F6F8FA") ?? .gray)
        privacyPolicyBtn.layer.cornerRadius = 8
        
        self.view.addSubview(logoView)
        self.view.addSubview(nameLabel)
        self.view.addSubview(versionLabel)
        self.view.addSubview(termsBtn)
        self.view.addSubview(privacyPolicyBtn)
        
        logoView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(40)
            make.centerX.equalToSuperview()
            make.width.height.equalTo(88)
        }
        
        nameLabel.snp.makeConstraints { make in
            make.top.equalTo(logoView.snp.bottom).offset(12)
            make.centerX.equalToSuperview()
        }
        
        versionLabel.snp.makeConstraints { make in
            make.top.equalTo(nameLabel.snp.bottom).offset(14)
            make.centerX.equalToSuperview()
        }
        
        termsBtn.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.top.equalTo(versionLabel.snp.bottom).offset(68)
            make.height.equalTo(48)
        }
        
        privacyPolicyBtn.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.top.equalTo(termsBtn.snp.bottom).offset(16)
            make.height.equalTo(48)
        }
    }
 
    @objc private func termsBtnClicked(_ sender: UIButton) {
        ARWebDefine.presentSFVc(ARWebDefine.url.termOfUse.rawValue, at: self)
    }
    
    @objc private func privacyPolicyBtnClicked(_ sender: UIButton) {
        ARWebDefine.presentSFVc(ARWebDefine.url.privacyPolicy.rawValue, at: self)
    }
}
