//
//  ARMyGiftController.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/22.
//

import UIKit
import SnapKit

class ARMyGiftController: ARBaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    

    private func setupUI() {
        
        // nav
        self.ar_setupNavBgImg()
        self.ar_configWhiteBackItem()
        self.title = ARHelper.localString("My Gifts")
        if let navigationBar = self.navigationController?.navigationBar {
            navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: ARHelper.font(18, weight: .medium)]
        }
        
        if let userId = ARUserManager.shared.userId {
            let view = ARReciveGiftView(userId: userId)
            self.view.addSubview(view)
            view.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(22)
                make.centerX.equalToSuperview()
            }
        }
    }

}
