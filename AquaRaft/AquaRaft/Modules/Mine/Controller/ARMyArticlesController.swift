//
//  ARMyArticlesController.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/22.
//

import UIKit
import SnapKit

class ARMyArticlesController: ARBaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    

    private func setupUI() {
        
        // nav
        self.ar_setupNavBgImg()
        self.ar_configWhiteBackItem()
        self.title = ARHelper.localString("My Articles")
        if let navigationBar = self.navigationController?.navigationBar {
            navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: ARHelper.font(18, weight: .medium)]
        }
        
        if let userId = ARUserManager.shared.userId {
            
            let viewModel = ARMineArticlesViewModel(userId: userId)
            let vc = ARWorkWaterFlowController(viewModel: viewModel)
            vc.setupHeaderRefresherEnable(true)
            vc.waterFlowHeightRatio = [224.0 / 166]
            vc.shouldHideMoreBtnWhenWorkAuthorIsCurrentUser = false
            self.addChild(vc)
            self.view.addSubview(vc.view)
            vc.view.snp.makeConstraints { make in
                make.left.top.right.equalToSuperview()
                make.bottom.equalToSuperview().offset(-ARHelper.safeBottomHeight())
            }
            
            vc.loadData()
        }
    }

}
