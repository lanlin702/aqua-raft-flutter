//
//  ARMineBlockLIstOrFollowListController.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/23.
//

import UIKit
import MJRefresh
import SnapKit

class ARMineBlockLIstOrFollowListController: ARBaseViewController {

    enum OperationType {
        case blockList, followList
    }
    
    private static let cellReuseId = "ar.blockListOrFollowListVc.cellReuseId"
    
    let operationType: OperationType
    private let viewModel: ARMineBlockListOrFollowListViewModel
    
    init(operationType: OperationType) {
        self.operationType = operationType
        self.viewModel = ARMineBlockListOrFollowListViewModel(operationType: operationType)
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }
    
    private func setupUI() {
        
        self.ar_setupNavBgImg()
        self.ar_configWhiteBackItem()
        switch operationType {
        case .blockList:
            self.title = ARHelper.localString("Block List")
        case .followList:
            self.title = ARHelper.localString("Follow List")
        }
        if let navigationBar = self.navigationController?.navigationBar {
            navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: ARHelper.font(18, weight: .medium)]
        }
        
        self.view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.left.right.equalToSuperview()
            make.top.bottom.equalToSuperview()
        }
    }

    @objc func loadData() {
        if self.refreshHeader.isRefreshing == false {
            self.ar.showLoading()
        }
        viewModel.loadData { [weak self] err in
            self?.ar.hideHUD()
            self?.refreshHeader.endRefreshing()
            if let err = err {
                self?.ar.makeToast(err.localizedDescription)
                return
            }
            self?.tableView.reloadData()
        }
    }
    
    private lazy var tableView: UITableView = {
        var tableView = UITableView.init(frame: .zero, style: .plain)
        tableView.backgroundColor = .clear
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = true
        tableView.register(ARBlackListAndFollowUserCell.self, forCellReuseIdentifier: Self.cellReuseId)
        tableView.estimatedRowHeight = 76
        tableView.mj_header = self.refreshHeader
        
        tableView.contentInset = .init(top: 12, left: 0, bottom: ARHelper.safeBottomHeight(), right: 0)
        
        return tableView
    }()
    
    private lazy var refreshHeader: ARRefreshHeader = {
        let header = ARRefreshHeader(refreshingTarget: self, refreshingAction: #selector(loadData))
        header.stateLabel?.isHidden = true
        header.lastUpdatedTimeLabel?.isHidden = true
        return header
    }()
    
}

extension ARMineBlockLIstOrFollowListController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // 不会经常变动，这里就不做判断
        let model = self.viewModel.dataSource[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: Self.cellReuseId, for: indexPath) as! ARBlackListAndFollowUserCell
        cell.selectionStyle = .none
        cell.updateValue(model, operationType: self.operationType)
        cell.operationCallback = { [weak self] aCell, type in
            self?.ar.showLoading()
            self?.viewModel.operationAction(model.userId, completion: { err in
                self?.ar.hideHUD()
                if let err = err {
                    self?.ar.makeToast(err.localizedDescription)
                    return
                }
                // 刷新
//                self?.tableView.reloadData()
                self?.loadData()
            })
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76.0
    }
    
}
