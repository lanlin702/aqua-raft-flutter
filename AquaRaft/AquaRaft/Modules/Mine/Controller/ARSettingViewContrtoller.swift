//
//  ARSettingViewContrtoller.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/17.
//

import UIKit
import SnapKit
import AttributedString

class ARSettingViewContrtoller: ARBaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    

    private func setupUI() {
        
        // nav
        self.ar_setupNavBgImg()
        self.ar_configWhiteBackItem()
        self.title = ARHelper.localString("Setting")
        if let navigationBar = self.navigationController?.navigationBar {
            navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: ARHelper.font(18, weight: .medium)]
        }
        
        let idView = UIView()
        idView.backgroundColor = (UIColor(hexString: "#F6F8FA") ?? .gray)
        idView.layer.cornerRadius = 8
        
        let idLabel = UILabel()
        idLabel.font = ARHelper.font(16, weight: .regular)
        idLabel.textColor = .init(hexString: "#404040")
        idLabel.text = "ID: \(ARUserManager.shared.userId ?? "")"
        
        let copyBtn: UIButton = {
            let btn = UIButton(type: .custom)
            btn.addTarget(self, action: #selector(copyIdBtnClicked(_:)), for: .touchUpInside)
            btn.setTitle(ARHelper.localString("Copy"), for: .normal)
            btn.backgroundColor = .white
            btn.setTitleColor(.init(hexString: "#17AA37"), for: .normal)
            btn.layer.cornerRadius = 8
            btn.titleLabel?.font = ARHelper.font(10, weight: .regular)
            return btn
        }()
        
        let autoTranslateView = UIView()
        autoTranslateView.backgroundColor = (UIColor(hexString: "#F6F8FA") ?? .gray)
        autoTranslateView.layer.cornerRadius = 8
        
        let autoTranslateLabel = UILabel()
        autoTranslateLabel.font = ARHelper.font(16, weight: .regular)
        autoTranslateLabel.textColor = .init(hexString: "#404040")
        autoTranslateLabel.text = ARHelper.localString("Auto translate")
        
        let switchView: UISwitch = {
            let btn = UISwitch()
            btn.addTarget(self, action: #selector(autoTranslateSwitchValueDidChange(_:)), for: .valueChanged)
            btn.tintColor = UIColor(hexString: "#86FB8C")
            btn.isOn = ARConfigManager.share.autoTranslate
            return btn
        }()
        
        self.view.addSubview(idView)
        idView.addSubview(idLabel)
        idView.addSubview(copyBtn)
        
        self.view.addSubview(autoTranslateView)
        autoTranslateView.addSubview(autoTranslateLabel)
        autoTranslateView.addSubview(switchView)
        
        self.view.addSubview(deleteBtn)
        self.view.addSubview(logoutBtn)
        
        idView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.top.equalToSuperview().offset(32)
            make.height.equalTo(48)
        }
        
        idLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(12)
        }
        
        copyBtn.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-12)
            make.width.equalTo(35)
            make.height.equalTo(16)
        }
        
        autoTranslateView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.top.equalTo(idView.snp.bottom).offset(16)
            make.height.equalTo(48)
        }
        
        autoTranslateLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(12)
            make.centerY.equalToSuperview()
        }
        
        switchView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-12)
        }
        
        deleteBtn.snp.makeConstraints { make in
            make.left.right.equalToSuperview()
            make.height.equalTo(14)
            make.bottom.equalToSuperview().offset(-ARHelper.heightScale(140) - ARHelper.safeBottomHeight())
        }
        
        logoutBtn.snp.makeConstraints { make in
            make.height.equalTo(48)
            make.width.equalTo(243)
            make.centerX.equalToSuperview()
            make.bottom.equalTo(deleteBtn.snp.top).offset(-20)
        }
        
    }

    @objc private func copyIdBtnClicked(_ sender: UIButton) {
        
        guard let userId = ARUserManager.shared.userId else {
            return
        }
        
        UIPasteboard.general.string = userId
        self.ar.makeToast(ARHelper.localString("Copy successful!"))
    }
    
    @objc private func autoTranslateSwitchValueDidChange(_ sender: UISwitch) {
        
        ARConfigManager.share.autoTranslate = sender.isOn
    }
    
    @objc private func logoutBtnClicked(_ sender: UIButton) {
        
        ARLogManager.logPage(.logout)
        
        self.ar.showLoading()
        ARUserManager.shared.logout { [weak self] err in
            self?.ar.hideHUD()
            if let err = err {
                self?.ar.makeToast(err.localizedDescription)
                return
            }
            
            ARAppLauncher.share.transitionToLoginPage()
        }
    }
    
    @objc private func deleteBtnClicked(_ sender: UIButton) {
        
        ARLogManager.logPage(.deleteaccount)
        
        let yes = UIAlertAction(title: ARHelper.localString("Yes"), style: .default) { [weak self] action in
            self?.__deleteAccount()
        }
        let cancel = UIAlertAction(title: ARHelper.localString("Return"), style: .destructive)
        let alert = UIAlertController(title: nil, message: ARHelper.localString("Are you sure you want to delete the account"), preferredStyle: .alert)
        alert.addAction(cancel)
        alert.addAction(yes)
        self.present(alert, animated: true)
    }
    
    private func __deleteAccount() {
        self.ar.showLoading()
        ARUserManager.shared.deleteAccount { [weak self] err in
            self?.ar.hideHUD()
            if let err = err {
                self?.ar.makeToast(err.localizedDescription)
                return
            }
            
            ARAppLauncher.share.transitionToLoginPage()
        }
    }
    
    // Lazy
    
    private lazy var logoutBtn: UIButton = {
        let btn = UIButton.ar.themeStyleButton()
        btn.addTarget(self, action: #selector(logoutBtnClicked(_:)), for: .touchUpInside)
        btn.setTitle(ARHelper.localString("Logout"), for: .normal)
        return btn
    }()
    
    private lazy var deleteBtn: UIButton = {
        let btn = UIButton(type: .custom)
        btn.addTarget(self, action: #selector(deleteBtnClicked(_:)), for: .touchUpInside)
        
        let attri = ASAttributedString(string: ARHelper.localString("Delete Account"), .font(ARHelper.font(14, weight: .regular)), .foreground(UIColor(hexString: "#FF5065") ?? .black), .underline(.single, color: UIColor(hexString: "#FF5065"))).value
        btn.setAttributedTitle(attri, for: .normal)
        
        return btn
    }()
}
