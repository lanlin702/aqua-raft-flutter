//
//  AREditUserInfoApi.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/19.
//

import Foundation
import Alamofire

class ARUpdateAvatarRequest: ARRequest {
    
    override var api: String {
        "/user/updateAvatar"
    }
    
    override var httpMethod: HTTPMethod {
        .post
    }
    
    override var encoder: ParameterEncoding {
        JSONEncoding.default
    }
    
    func updateAvatar(_ avatarPath: String, completion: @escaping (ARRequestResult<ARUpdateAvatarResponse>) -> Void) {
        
        self.parameters = [
            "avatarPath" : avatarPath
        ]
        self.request(of: ARUpdateAvatarResponse.self) { response, result in
            completion(result)
        }
    }
    
}

class ARUpdateUserInfoRequest: ARRequest {
    
    override var api: String {
        "/user/saveUserInfo"
    }
    
    override var httpMethod: HTTPMethod {
        .post
    }
    
    override var encoder: ParameterEncoding {
        JSONEncoding.default
    }
    
    func updateUserInfo(birthday: String, country: String, nickname: String, completion: @escaping (Error?) -> Void) {
        
        self.parameters = [
            "birthday" : birthday,
            "country" : country,
            "nickname" : nickname,
        ]
        self.request { response, result in
            switch result {
            case .faild(let err):
                completion(err)
            case .success(_), .successWithoutResponse:
                completion(nil)
            }
        }
    }
    
}

// MARK: - Response model

/// 回调
class ARUpdateAvatarResponse: Decodable {
    
    let mediaPath: String
    let mediaUrl: String
    let thumbUrl: String
    let middleThumbUrl: String
    
}
