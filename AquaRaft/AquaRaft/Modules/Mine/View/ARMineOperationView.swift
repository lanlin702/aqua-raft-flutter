//
//  ARMineOperationView.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/17.
//

import UIKit
import SnapKit

class ARMineOperationView: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        self.backgroundColor = .white
        self.layer.cornerRadius = 12
        self.layer.shadowColor = (UIColor(hexString: "#064B69") ?? .black).cgColor
        self.layer.shadowOpacity = 0.1
        self.layer.shadowOffset = .init(width: 3, height: 3)
        
        self.addSubview(iconView)
        self.addSubview(textLabel)
        self.addSubview(arrowView)
        
        iconView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(20)
        }
        
        textLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalTo(iconView.snp.right).offset(10)
        }
        
        arrowView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-20)
        }
    }
    
    lazy var iconView: UIImageView = {
        let view = UIImageView()
        return view
    }()
    
    lazy var textLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hexString: "#404040")
        label.font = .systemFont(ofSize: 14, weight: .regular)
        return label
    }()
    
    private lazy var arrowView: UIImageView = {
        let view = UIImageView(image: .init(named: "mine_arrow"))
        return view
    }()
}
