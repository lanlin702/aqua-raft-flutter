//
//  AREditUserInfoOperationView.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/20.
//

import UIKit
import SnapKit

class AREditUserInfoOperationView: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        self.backgroundColor = .white
        
        self.addSubview(leftLabel)
        self.addSubview(rightLabel)
        self.addSubview(arrowView)
        
        leftLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.centerY.equalToSuperview()
        }
        
        rightLabel.snp.makeConstraints { make in
            make.right.equalToSuperview().offset(-20)
            make.centerY.equalToSuperview()
        }
        
        arrowView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-20)
            make.width.equalTo(7)
            make.height.equalTo(12)
        }
    }
    
    func updateArrowHidden(_ isHidden: Bool) {
        arrowView.isHidden = isHidden
        rightLabel.snp.updateConstraints { make in
            make.right.equalToSuperview().offset(-20 - (isHidden ? 0 : 7 + 5))
        }
    }
    
    lazy var leftLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black.withAlphaComponent(0.3)
        label.font = .systemFont(ofSize: 12, weight: .regular)
        label.textAlignment = .left
        return label
    }()
    
    lazy var rightLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hexString: "#404040")
        label.font = .systemFont(ofSize: 16, weight: .regular)
        label.textAlignment = .right
        return label
    }()
    
    private lazy var arrowView: UIImageView = {
        let view = UIImageView(image: .init(named: "common_rightArrow"))
        view.isHidden = true
        return view
    }()
}
