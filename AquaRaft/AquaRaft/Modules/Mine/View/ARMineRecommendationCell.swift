//
//  ARMineRecommendationCell.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/23.
//

import UIKit
import JKSwiftExtension
import SnapKit

class ARMineRecommendationCell: UITableViewCell {

    var operationCallback: ((ARMineRecommendationCell) -> Void)?
    var work: ARWorkModel?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    // MARK: -
    
    private func setupUI() {
        
        let shadowView = UIView()
        shadowView.backgroundColor = .white
        shadowView.layer.cornerRadius = 20
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOpacity = 0.1
        shadowView.layer.shadowOffset = .init(width: 3, height: 3)
        
        self.contentView.addSubview(shadowView)
        self.contentView.addSubview(containerView)
        
        containerView.layer.addSublayer(gradientLayer)
        containerView.addSubview(avatarView)
        containerView.addSubview(nicknameLabel)
        containerView.addSubview(deleteBtn)
        containerView.addSubview(titleLabel)
        containerView.addSubview(coverImageView)
        containerView.addSubview(reviewSignView)
        
        shadowView.snp.makeConstraints { make in
            make.edges.equalTo(containerView)
        }
        
        containerView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.top.equalToSuperview()
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview().offset(-16)
        }
        avatarView.snp.makeConstraints { make in
            make.width.height.equalTo(40)
            make.top.left.equalToSuperview().offset(16)
        }
        deleteBtn.snp.makeConstraints { make in
            make.width.height.equalTo(30)
            make.centerY.equalTo(avatarView)
            make.right.equalToSuperview().offset(-16)
        }
        
        nicknameLabel.snp.makeConstraints { make in
            make.left.equalTo(avatarView.snp.right).offset(10)
            make.right.equalTo(deleteBtn.snp.left).offset(-10)
            make.centerY.equalTo(avatarView)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.left.equalTo(avatarView)
            make.right.equalTo(deleteBtn)
            make.top.equalTo(avatarView.snp.bottom).offset(16)
            make.height.equalTo(14)
        }
        
        coverImageView.snp.makeConstraints { make in
            make.left.right.bottom.equalToSuperview()
            make.top.equalTo(titleLabel.snp.bottom).offset(16)
        }
        
        reviewSignView.snp.makeConstraints { make in
            make.right.equalToSuperview().offset(-16)
            make.bottom.equalToSuperview().offset(-16)
            make.height.equalTo(28)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = containerView.bounds
    }
    
    // MARK: -
    
    @objc private func operationBtnClicked(_ sender: UIButton) {
        operationCallback?(self)
    }
    
    func updateValue(_ work: ARWorkModel) {
        self.work = work
        
        avatarView.image = nil
        if let url = work.author?.trueAvatar {
            avatarView.ar.setImage(url)
        }
        nicknameLabel.text = work.author?.nickname
        titleLabel.text = work.workTitle
        coverImageView.image = nil
        if let url = work.realPicCover() {
            coverImageView.ar.setImage(url)
        }
    }
    
    // MARK: -
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 20
        view.layer.masksToBounds = true
        return view
    }()
    
    private lazy var avatarView: UIImageView = {
        let avatarView = UIImageView()
        avatarView.layer.cornerRadius = 20
        avatarView.layer.masksToBounds = true
        avatarView.backgroundColor = .gray.withAlphaComponent(0.8)
        return avatarView
    }()
    
    private lazy var nicknameLabel: UILabel = {
        let label = UILabel()
        label.font = ARHelper.font(14, weight: .regular)
        label.textColor = UIColor(hexString: "#404040")
        return label
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = ARHelper.font(12, weight: .regular)
        label.textColor = UIColor.black.withAlphaComponent(0.6)
        return label
    }()
    
    private lazy var deleteBtn: UIButton = {
        let btn = UIButton(type: .custom)
        btn.addTarget(self, action: #selector(operationBtnClicked(_:)), for: .touchUpInside)
        btn.setImage(.init(named: "mine_work_delete"), for: .normal)
        return btn
    }()
    
    private lazy var coverImageView: UIImageView = {
        let coverImageView = UIImageView()
        return coverImageView
    }()
    
    private lazy var reviewSignView: UIView = {
        var view = UIView()
        view.jk.height = 28
        view.layer.cornerRadius = 14
        view.layer.masksToBounds = true
        view.layer.borderColor = UIColor.white.cgColor
        view.layer.borderWidth = 1
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor.init(hexString: "#39BAE8")!.cgColor, UIColor.init(hexString: "#5892E9")!.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
        
        view.addSubview(reviewSignLabel)
        reviewSignLabel.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        view.layer.insertSublayer(gradientLayer, at: 0)
        gradientLayer.frame = reviewSignLabel.bounds
        
        return view
    }()
    
    private lazy var reviewSignLabel: JKPaddingLabel = {
        var label = JKPaddingLabel()
        label.paddingLeft = 20
        label.paddingRight = 20
        label.text = ARHelper.localString("Under review")
        label.font = ARHelper.font(14, weight: .regular)
        label.textColor = .white
        label.sizeToFit()
        label.jk.height = 28
        
        return label
    }()
    
    private lazy var gradientLayer: CAGradientLayer = {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor.init(hexString: "#FFFFFF")!.cgColor, UIColor.init(hexString: "#DCF7FF")!.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
        return gradientLayer
    }()
    
}
