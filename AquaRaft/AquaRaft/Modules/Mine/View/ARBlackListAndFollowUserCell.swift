//
//  ARBlackListAndFollowUserCell.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/23.
//

import UIKit
import SnapKit
import JKSwiftExtension

class ARBlackListAndFollowUserCell: UITableViewCell {

    var operationCallback: ((ARBlackListAndFollowUserCell, ARMineBlockLIstOrFollowListController.OperationType) -> Void)?
    
    private var operationType: ARMineBlockLIstOrFollowListController.OperationType = .blockList
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: -
    
    private func setupUI() {
        self.contentView.addSubview(avatarView)
        self.contentView.addSubview(nicknameLabel)
        self.contentView.addSubview(infoLabel)
        self.contentView.addSubview(operationBtn)
        
        avatarView.snp.makeConstraints { make in
            make.width.height.equalTo(44)
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(20)
        }
        
        nicknameLabel.snp.makeConstraints { make in
            make.left.equalTo(avatarView.snp.right).offset(10)
            make.right.equalTo(operationBtn.snp.left).offset(-10)
            make.top.equalTo(avatarView).offset(4)
            make.height.equalTo(16)
        }
        
        infoLabel.snp.makeConstraints { make in
            make.left.right.equalTo(nicknameLabel)
            make.height.equalTo(14)
            make.bottom.equalTo(avatarView).offset(-2)
        }
        
        operationBtn.snp.makeConstraints { make in
            make.width.equalTo(70)
            make.height.equalTo(26)
            make.right.equalToSuperview().offset(-20)
            make.centerY.equalToSuperview()
        }
    }
    
    // MARK: -
    
    @objc private func operationBtnClicked(_ sender: UIButton) {
        operationCallback?(self, operationType)
    }
    
    func updateValue(_ user: ARUserModel, operationType: ARMineBlockLIstOrFollowListController.OperationType) {
        self.operationType = operationType
        
        avatarView.image = nil
        avatarView.ar.setImage(user.trueAvatar)
        nicknameLabel.text = user.nickname
        // info
        infoLabel.text = "\(user.age) \(user.country)"
        
        switch operationType {
        case .blockList:
            operationBtn.backgroundColor = .init(hexString: "#454545")
            operationBtn.setTitleColor(.white, for: .normal)
            operationBtn.setTitle(ARHelper.localString("Unblock"), for: .normal)
        case .followList:
            operationBtn.backgroundColor = .init(hexString: "#F2F2F2")
            operationBtn.setTitleColor(.init(hexString: "#CCCCCC"), for: .normal)
            operationBtn.setTitle(ARHelper.localString("Unfollow"), for: .normal)
        }
    }
    
   // MARK: -
    
    private lazy var avatarView: UIImageView = {
        let avatarView = UIImageView()
        avatarView.layer.cornerRadius = 22
        avatarView.layer.masksToBounds = true
        avatarView.backgroundColor = .gray.withAlphaComponent(0.8)
        return avatarView
    }()

    private lazy var nicknameLabel: UILabel = {
        let label = UILabel()
        label.font = ARHelper.font(14, weight: .regular)
        label.textColor = UIColor(hexString: "#404040")
        return label
    }()

    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.font = ARHelper.font(12, weight: .regular)
        label.textColor = UIColor.black.withAlphaComponent(0.6)
        return label
    }()
    
    private lazy var operationBtn: UIButton = {
        let btn = UIButton(type: .custom)
        btn.addTarget(self, action: #selector(operationBtnClicked(_:)), for: .touchUpInside)
        btn.titleLabel?.font = ARHelper.font(12, weight: .regular)
        btn.layer.cornerRadius = 13
        return btn
    }()
}
