//
//  ARUserRelationshipApi.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/21.
//

import Foundation
import Alamofire

/// 关注、取消关注
class ARFollowOrUnfollowRequest: ARRequest {
    
    private var followOrUnfollow: Bool = false
    
    override var api: String {
        if followOrUnfollow {
            return "/user/addFriend"
        }
        return "/user/unfriend"
    }
    
    override var httpMethod: HTTPMethod {
        .post
    }
    
    func followOrUnfollowUser(_ userId: String, followOrUnfollow: Bool, completion: @escaping (Error?) -> Void) {
        self.followOrUnfollow = followOrUnfollow
        self.parameters = [
            "followUserId" : userId
        ]
        self.request { response, result in
            switch result {
            case .faild(let err):
                completion(err)
            case .successWithoutResponse, .success(_):
                completion(nil)
            }
        }
    }
    
}

/// 拉黑、取消拉黑
class ARAddOrRemoveBlackListRequest: ARRequest {
    
    private var addOrRemoveBlackList: Bool = false
    
    override var api: String {
        if addOrRemoveBlackList {
            return "/report/complain/insertRecord"
        }
        return "/report/complain/removeBlock"
    }
    
    override var httpMethod: HTTPMethod {
        .post
    }

    /// 拉黑、取消拉黑接口
    /// - Parameters:
    ///   - addOrRemoveBlackList: 拉黑或取消拉黑
    ///   - subReason: 如果有值，则默认是举报, addOrRemoveBlackList == false 时无效
    func addOrRemoveBlackListFor(_ userId: String, addOrRemoveBlackList: Bool, subReason: String? = nil, completion: @escaping (Error?) -> Void) {
        self.addOrRemoveBlackList = addOrRemoveBlackList
        if addOrRemoveBlackList {
            if let subReason = subReason {
                // 举报
                self.parameters = [
                    "broadcasterId" : userId,
                    "complainCategory" : "Report",
                    "complainSub" : subReason
                ]
            } else {
                // 拉黑
                self.parameters = [
                    "broadcasterId" : userId,
                    "complainCategory" : "Block"
                ]
            }
        } else {
            // 移除拉黑
            self.parameters = [
                "blockUserId" : userId
            ]
        }
        self.request { response, result in
            switch result {
            case .faild(let err):
                completion(err)
            case .successWithoutResponse, .success(_):
                completion(nil)
            }
        }
    }
    
}
