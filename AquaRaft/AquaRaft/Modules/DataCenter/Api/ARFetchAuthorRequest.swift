//
//  ARFetchAuthorRequest.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/7.
//

import UIKit
import Alamofire

/// 获取用户
class ARFetchAuthorRequest: ARRequest {

    override var api: String {
        "/broadcaster/wall/search"
    }
    
    override var httpMethod: HTTPMethod {
        .post
    }
    
    override var encoder: ParameterEncoding {
        JSONEncoding.default
    }
    
    func fetchAuthorList(_ completion: @escaping ([ARUserModel]?, ARRequestError?) -> Void) {
        
        self.parameters = [
            "isRemoteImageUrl" : true,
            "limit" : 20,
            "page" : 1,
            "isPageMode" : true
        ]
        self.request(of: [ARUserModel].self) { response, result in
            switch result {
            case .faild(let err):
                completion(nil, err)
            case .success(let model):
                completion(model, nil)
            case .successWithoutResponse:
                completion(nil, .modelConvertError(error: nil))
            }
        }
    }
}
