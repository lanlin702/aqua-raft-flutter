//
//  ARVideoWorkModel.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/4/20.
//

import Foundation

/// 视频流作品
@objcMembers
class ARVideoWorkModel: NSObject, Codable {
    
    var id: String
    var title: String
    
    // 作者
    var authorId: String?
    var author: ARUserModel?
    
    var comments: [ARCommentModel]?
    
    var shortPath: String
    
    var isLike: Bool
    
    var localCover: String
    var localPath: String
    
    /// 需要请求过后设置
    var videoUrl: String?
    
    @objc func isCurrentUser() -> Bool {
        return authorId ?? "" == ARUserManager.shared.userId ?? ""
    }
}
