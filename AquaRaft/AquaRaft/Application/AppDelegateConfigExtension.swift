//
//  AppDelegateConfigExtension.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/3/24.
//

import UIKit
import IQKeyboardManagerSwift

/// 一些初始化
extension AppDelegate {
    
    func configApplicationWhenDidFinishLaunch() {
        // 键盘的默认设置
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        
//        // tabbar - 透明效果
//        UITabBar.appearance().isTranslucent = false
//        UITabBar.appearance().barTintColor = .white
        
        // scrollView
        UIScrollView.appearance().showsHorizontalScrollIndicator = false
        UIScrollView.appearance().showsHorizontalScrollIndicator = false
        UIScrollView.appearance().contentInsetAdjustmentBehavior = .never
        
        // tableview
        UITableView.appearance().estimatedRowHeight = 0
        UITableView.appearance().estimatedSectionFooterHeight = 0
        UITableView.appearance().estimatedSectionHeaderHeight = 0
        UITableView.appearance().separatorStyle = .none
        UITableView.appearance().contentInsetAdjustmentBehavior = .never
        if #available(iOS 15.0, *) {
            UITableView.appearance().sectionHeaderTopPadding = 0
        }
        
        // collectionView
        UICollectionView.appearance().contentInsetAdjustmentBehavior = .never
    }
    
}
