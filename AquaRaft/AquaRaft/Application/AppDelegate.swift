//
//  AppDelegate.swift
//  AquaRaft
//
//  Created by 何启亮 on 2024/3/22.
//

import UIKit
import SwiftyStoreKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    /// 手动设置的window
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // idfa
        ARDevice.share.requestIdfa()
        
        // 初始化
        ARConfigManager.share.fetchAppConfigIfCan()
        let _ = ARDataCenter.default
        
        // 配置
        self.configApplicationWhenDidFinishLaunch()
        
        // window
        let window = UIWindow.init(frame: UIScreen.main.bounds)
        let launchVc = ARAppLaunchController.init(nibName: nil, bundle: nil)
        window.rootViewController = launchVc
        self.window = window
        window.makeKeyAndVisible()
        
        SwiftyStoreKit.completeTransactions { purchases in
            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased, .restored:
                    if purchase.needsFinishTransaction {
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                    // Unlock content
                case .failed, .purchasing, .deferred:
                    break // do nothing
                @unknown default:
                    break
                }
            }
        }
        
        return true
    }
}

