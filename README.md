### AquaRaft

#### 模块划分

* 入口模块: AppEntranceModule 
  * App didFinishLaunch 协议后，第一个初始化的界面，在这里需要判断是否第一次打开、接下来的登录逻辑、跳转页面的逻辑
* LoginModule:
  * 管理登录、用户相关
* DataCenterModule:
  * 鉴于首页、浏览、评论等数据都是做本地数据，故独立开一个模块来进行管理
* IndexModule:
  * 首页
* VideoFlowModule:
  * 视频流
* DiscoverModule:
  * 发现页
* MineModule:
  * 我的模块
* CommonModule:
  * 公共模块
* GiftModule:
  * 礼物模块，包含内购

#### 部分流程

*  App启动流程:

  * 由于 udid(Idfa/idfv)，是所有接口请求参数的前提，而新版的 idfa 获取是一个异步的方法，故这里会有一个标志位记录是否已获取 idfa，获取成功后才会进行下一步
  * 在启动界面，判断是否为第一次安装，如果是则跳转至新特性页面，否则判断是否有自动登录，如果是则进行自动登录，登录成功跳转至首页，否则跳转至登录页

  
